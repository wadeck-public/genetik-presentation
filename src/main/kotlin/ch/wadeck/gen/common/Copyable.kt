package ch.wadeck.gen.common

interface Copyable<out T> {
	fun copy(): T
}
