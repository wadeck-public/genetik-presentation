package ch.wadeck.gen.common

class IdGenerator {
	companion object {
		private var id = 0
		val next get() = ++id
	}
}
