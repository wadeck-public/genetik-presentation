package ch.wadeck.gen.common

abstract class Mutant<M : Mutant<M>> : Copyable<M> {
	val id: Int = IdGenerator.next

	/**
	 * business dependent value, will be normalized
	 */
	var fitnessScore: Double = Double.MIN_VALUE
	/**
	 * between 0 (worst) and 1 (best)
	 */
	var normalizedScore: Double = NORMALIZED_SCORE_NOT_SET

	/**
	 * Optional
	 */
	var parentId: Int? = null

	/**
	 * Optional
	 */
	var secondParentId: Int? = null

	/**
	 * Representation of the gen in a schematic way
	 */
	abstract val familyClass: String

	var source: Source = Source.UNKNOWN

	abstract fun getGeneAsString(): String

	abstract fun toSimplifiedString(): String

	abstract fun toExtendedString(): String

	fun resetScore() {
		this.fitnessScore = Double.MIN_VALUE
		this.normalizedScore = NORMALIZED_SCORE_NOT_SET
	}

	companion object {
		val NORMALIZED_SCORE_NOT_SET: Double = -1.0
	}
}
