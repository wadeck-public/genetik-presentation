package ch.wadeck.gen.common

enum class Source {
	UNKNOWN,
	BREED,
	COPY,
	RANDOM,
	MUTATION
}