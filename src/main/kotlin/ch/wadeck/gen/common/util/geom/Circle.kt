package ch.wadeck.gen.common.util.geom

interface Circle<out N : Number> {
	val centerX: N
	val centerY: N
	val radius: N
}

data class DoubleCircle(override val centerX: Double, override val centerY: Double, override val radius: Double) : Circle<Double>
data class IntCircle(override val centerX: Int, override val centerY: Int, override val radius: Int) : Circle<Int>
