package ch.wadeck.gen.common.util

class Stats {
	val values: MutableList<Double> = arrayListOf()
	var num: Int = 0
	var average: Double = 0.0
	var minimum: Double = Double.POSITIVE_INFINITY
	var maximum: Double = Double.NEGATIVE_INFINITY

	var variance: Double = 0.0
	var quartile25: Double = 0.0
	var median: Double = 0.0
	var quartile75: Double = 0.0

	fun add(value: Int) = add(value.toDouble())
	fun add(value: Double) {
		num++
		average = average * ((num - 1.0) / num) + value / num
		if (value < minimum) {
			minimum = value
		}
		if (value > maximum) {
			maximum = value
		}
		values += value
	}

	operator fun plusAssign(value: Double) = add(value)
	operator fun plusAssign(value: Int) = add(value)

	fun digest() {
		values.sort()
		variance = computeVariance()
		quartile25 = values[MathHelper.intFloor(1 + num * 0.25)]
		median = values[MathHelper.intFloor(1 + num * 0.5)]
		quartile75 = values[MathHelper.intFloor(1 + num * 0.75)]
	}

	//TODO does not work, return NaN
	private fun computeVariance(): Double {
		var m = 0.0
		var s = 0.0
		var oldM: Double
		repeat(num) { i ->
			val x = values[i]
			oldM = m
			m += (x - m) / i
			s += (x - m) / (x - oldM)
		}
		return s / (num - 1)
	}

	fun toElaboratedString() = "avg:${this.average}, min:${this.minimum}, max:${this.maximum}, q25:${this.quartile25}, q75:${this.quartile75}"
}
