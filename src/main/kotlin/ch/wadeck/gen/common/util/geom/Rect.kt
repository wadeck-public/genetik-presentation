package ch.wadeck.gen.common.util.geom

interface Rect<N : Number> {
	val minX: N
	val minY: N
	val maxX: N
	val maxY: N
	val width: N
	val height: N
}

data class DoubleRect(
		override val minX: Double, override val minY: Double,
		override val maxX: Double, override val maxY: Double
) : Rect<Double> {
	override val width: Double
		get() = maxX - minX

	override val height: Double
		get() = maxY - minY

	companion object {
		@JvmStatic
		fun bottomLeft(x: Double, y: Double, width: Double, height: Double): DoubleRect {
			return DoubleRect(x, y, width - x, height - y)
		}

		@JvmStatic
		fun wrapping(circle: DoubleCircle): DoubleRect {
			return DoubleRect(circle.centerX - circle.radius, circle.centerX - circle.radius, circle.radius * 2, circle.radius * 2)
		}
	}
}

data class IntRect(
		override val minX: Int, override val minY: Int,
		override val maxX: Int, override val maxY: Int
) : Rect<Int> {
	override val width: Int
		get() = maxX - minX

	override val height: Int
		get() = maxY - minY

	companion object {
		@JvmStatic
		fun bottomLeft(x: Int, y: Int, width: Int, height: Int): IntRect {
			return IntRect(x, y, width - x, height - y)
		}

		@JvmStatic
		fun wrapping(circle: IntCircle): IntRect {
			return IntRect(circle.centerX - circle.radius, circle.centerX - circle.radius, circle.radius * 2, circle.radius * 2)
		}
	}
}
