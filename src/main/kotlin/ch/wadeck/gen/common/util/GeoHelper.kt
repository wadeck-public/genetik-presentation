package ch.wadeck.gen.common.util

import ch.wadeck.gen.common.util.geom.DoublePoint
import ch.wadeck.gen.common.util.geom.DoubleRect
import ch.wadeck.gen.common.util.geom.IntCircle
import ch.wadeck.gen.common.util.geom.IntPoint

object GeoHelper {
	fun contains(circle: IntCircle, p: IntPoint) =
		squaredDistanceToCenter(circle, p) < circle.radius * circle.radius

	fun contains(rect: DoubleRect, p: DoublePoint) =
		rect.minX < p.x && rect.maxX > p.x && rect.minY < p.y && rect.maxY > p.y

	fun squaredDistance(a: DoublePoint, b: DoublePoint): Double =
		(a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)

	fun distance(a: DoublePoint, b: DoublePoint): Double =
		Math.sqrt(squaredDistance(a, b))

	fun squaredDistanceToCenter(circle: IntCircle, point: IntPoint): Int {
		val deltaX = point.x - circle.centerX
		val deltaY = point.y - circle.centerY
		return deltaX * deltaX + deltaY * deltaY
	}

	fun distanceToCenter(circle: IntCircle, point: IntPoint): Double =
		Math.sqrt(squaredDistanceToCenter(circle, point).toDouble())

	fun intersect(a: DoublePoint, b: DoublePoint, rect: DoubleRect): Boolean {
		if (a.x <= rect.minX && b.x <= rect.minX ||
			a.y <= rect.minY && b.y <= rect.minY ||
			a.x >= rect.maxX && b.x >= rect.maxX ||
			a.y >= rect.maxY && b.y >= rect.maxY) {
			// Completely outside.
			return false
		}

		val m = (b.y - a.y) / (b.x - a.x)

		var y = m * (rect.minX - a.x) + a.y
		if (y > rect.minY && y < rect.maxY)
			return true

		y = m * (rect.maxX - a.x) + a.y
		if (y > rect.minY && y < rect.maxY)
			return true

		var x = (rect.minY - a.y) / m + a.x
		if (x > rect.minX && x < rect.maxX)
			return true

		x = (rect.maxY - a.y) / m + a.x
		return x > rect.minX && x < rect.maxX
	}
}


