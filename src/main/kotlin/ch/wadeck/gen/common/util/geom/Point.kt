package ch.wadeck.gen.common.util.geom

interface Point<out N : Number> {
	val x: N
	val y: N
}

data class DoublePoint(override val x: Double, override val y: Double) : Point<Double> {
	companion object {
		val zero = DoublePoint(0.0, 0.0)
	}
}

data class IntPoint(override val x: Int, override val y: Int) : Point<Int> {
	companion object {
		val zero = IntPoint(0, 0)
	}
}
