package ch.wadeck.gen.common.util

object AssertHelper {
	/**
	 * [min, max[
	 *
	 * @param min inclusive
	 * @param max exclusive
	 */
	fun inRange(value: Int, min: Int, max: Int) {
		if (value < min) {
			throw AssertException("$value < $min")
		}
		if (value >= max) {
			throw AssertException("$value >= $max")
		}
	}

	fun isStrictlyBetween01(value: Float) {
		if (value <= 0) {
			throw AssertException("$value <= 0")
		}
		if (value >= 1) {
			throw AssertException("$value >= 1")
		}
	}

	fun isStrictlyBetween01(value: Double) {
		if (value <= 0) {
			throw AssertException("$value <= 0")
		}
		if (value >= 1) {
			throw AssertException("$value >= 1")
		}
	}

	fun isBetween01(value: Float) {
		if (value < 0) {
			throw AssertException("$value < 0")
		}
		if (value > 1) {
			throw AssertException("$value > 1")
		}
	}

	fun isBetween01(value: Double) {
		if (value < 0) {
			throw AssertException("$value < 0")
		}
		if (value > 1) {
			throw AssertException("$value > 1")
		}
	}

	fun isStrictlyPositive(value: Int) {
		if (value <= 0) {
			throw AssertException("$value <= 0")
		}
	}

	fun isStrictlyPositive(value: Float) {
		if (value <= 0) {
			throw AssertException("$value <= 0")
		}
	}

	fun isPositive(value: Int) {
		if (value < 0) {
			throw AssertException("$value < 0")
		}
	}

	fun isPositive(value: Float) {
		if (value < 0) {
			throw AssertException("$value < 0")
		}
	}

	fun isEqual(actual: Int, expected: Int) {
		if (actual != expected) {
			throw AssertException("$actual != $expected")
		}
	}

	fun isNull(o: Any?) {
		if (o != null) {
			throw AssertException("$o not null")
		}
	}

	fun isNotNull(o: Any?) {
		if (o == null) {
			throw AssertException("$o is null")
		}
	}

	fun greaterThan(a:Double, b:Double) {
		if (a <= b) {
			throw AssertException("$a <= $b")
		}
	}

	fun isTrue(b:Boolean) {
		if (!b) {
			throw AssertException("$b != true")
		}
	}

	fun isFalse(b:Boolean) {
		if (b) {
			throw AssertException("$b != false")
		}
	}

	fun fail(message: String): Nothing = throw AssertException(message)
}


