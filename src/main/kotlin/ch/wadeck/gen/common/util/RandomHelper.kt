package ch.wadeck.gen.common.util

import ch.wadeck.gen.common.util.geom.*
import java.util.*

object RandomHelper {
	fun randomRound(value: Double, random: Random = Random()): Int {
		val intValue = Math.floor(value).toInt()
		val remainingValue = value - intValue

		if (random.nextFloat() < remainingValue) {
			return intValue + 1
		}

		return intValue
	}

	fun randomRange(min: Int, max: Int, except: Int, random: Random = Random()): Int {
		if (min == except && max - min <= 1) {
			AssertHelper.fail("We cannot find another value inside [$min, $max]")
		}

		var current = random.nextInt(max - min) + min
		while (current == except) {
			current = random.nextInt(max - min) + min
		}

		return current
	}

	fun <T> randomSelect(random: Random = Random(), vararg builders: () -> T) =
		pickSingleRandomFrom(builders.toList(), random)()

	fun <T> pickSingleRandomFrom(list: List<T>, random: Random = Random()): T =
		list[random.nextInt(list.size)]

	fun <T> pickRandomFrom(list: List<T>, numToCollect: Int, random: Random = Random()): List<T> {
		AssertHelper.inRange(numToCollect, 0, list.size)

		val indices: MutableSet<Int> = mutableSetOf()
		while (indices.size < numToCollect) {
			indices.add(random.nextInt(list.size))
		}

		return indices.map { list[it] }
	}

	fun randomList(min: Int, max: Int, numOfElement: Int, random: Random = Random()): List<Int> {
		val values: MutableSet<Int> = mutableSetOf()
		while (values.size < numOfElement) {
			values.add(random.nextInt(min + max) - min)
		}

		return values.toList()
	}

	fun <T> pickWeightedSingleRandomFrom(vararg list: Pair<Double, T>, random: Random = Random()): T =
		pickWeightedSingleRandomFrom(list.toList(), random)

	fun <T> pickWeightedSingleRandomFrom(list: List<Pair<Double, T>>, random: Random = Random()) =
		pickWeightedSinglePairRandomFrom(list, random).second

	fun <T> pickWeightedSinglePairRandomFrom(list: List<Pair<Double, T>>, random: Random = Random()): Pair<Double, T> {
		val total: Double = list.sumByDouble { it.first }
		val randomValue: Double = random.nextDouble() * total

		var remainingPoints: Double = randomValue
		val selected: Pair<Double, T> = list.find {
			remainingPoints -= it.first
			remainingPoints <= 0
		} ?: list.last()

		return selected
	}

	fun <W> pickWeightedSingleRandomFrom(list: List<W>, random: Random = Random(), weightRetriever: (W) -> Double): W {
		val total = list.sumByDouble(weightRetriever)

		val randomValue = random.nextDouble() * total

		var remainingPoints = randomValue

		return list.find {
			remainingPoints -= weightRetriever(it)
			remainingPoints <= 0
		} ?: list.last()
	}

	fun randomPointInRect(rectangle: IntRect, random: Random = Random()): IntPoint {
		return IntPoint(
			rectangle.minX + random.nextInt(rectangle.maxX - rectangle.minX),
			rectangle.minY + random.nextInt(rectangle.maxY - rectangle.minY)
		)
	}

	fun randomPointInRect(rectangle: DoubleRect, random: Random = Random()): DoublePoint {
		return DoublePoint(
			rectangle.minX + random.nextDouble() * (rectangle.maxX - rectangle.minX),
			rectangle.minY + random.nextDouble() * (rectangle.maxY - rectangle.minY)
		)
	}

	fun randomPointInCircle(circle: IntCircle, random: Random = Random()): IntPoint {
		val rect: IntRect = IntRect.wrapping(circle)
		var point: IntPoint = randomPointInRect(rect, random)
		while (!GeoHelper.contains(circle, point)) {
			point = randomPointInRect(rect, random)
		}
		return point
	}

	fun randomPointInCircle(circle: DoubleCircle, random: Random = Random()): DoublePoint {
		val pointRadius = Math.sqrt(random.nextDouble() * circle.radius)
		val angle = Math.PI * 2 * random.nextDouble()
		return DoublePoint(
			pointRadius * Math.cos(angle),
			pointRadius * Math.sin(angle)
		)
	}

	fun <T> randomlyApplyUntilAtLeastOneValid(random: Random = Random(), vararg closures: Pair<Double, () -> T?>): T? =
		randomlyApplyUntilAtLeastOneValid(closures.asList(), random)

	fun <T> randomlyApplyUntilAtLeastOneValid(closures: List<Pair<Double, () -> T?>>, random: Random = Random()): T? {
		val potentialElements = closures.toMutableList()
		var total: Double = closures.sumByDouble { it.first }
		while (potentialElements.isNotEmpty()) {
			val randomValue: Double = random.nextDouble() * total

			var remainingPoints: Double = randomValue
			var index = potentialElements.indexOfFirst {
				remainingPoints -= it.first
				remainingPoints <= 0
			}
			if (index == -1) {
				// in case all elements have 0 weight
				index = potentialElements.lastIndex
			}
			val selectedPair = potentialElements.removeAt(index)
			val potentialResult = selectedPair.second()
			if (potentialResult != null) {
				return potentialResult
			} else {
				total -= selectedPair.first
			}
		}

		return null
	}
}
