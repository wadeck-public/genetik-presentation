package ch.wadeck.gen.common.util

import java.lang.RuntimeException

class AssertException(override val message: String) : RuntimeException(message)