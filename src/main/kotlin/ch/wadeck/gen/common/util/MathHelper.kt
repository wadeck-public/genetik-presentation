package ch.wadeck.gen.common.util

import org.opentest4j.AssertionFailedError
import java.math.BigDecimal
import kotlin.math.roundToInt

object MathHelper {
	val ROUND = 4
	val EPSILON by lazy { Math.pow(10.0, -ROUND.toDouble()) }
	val LOG2 by lazy { Math.log(2.0) }

	fun log2(value: Double) =
		Math.log(value) / LOG2

	fun approx(value: Double, precision: Int = ROUND) =
		Math.floor(value * Math.pow(10.0, precision.toDouble()) + 0.5) / Math.pow(10.0, precision.toDouble())

	fun reducePrecision(value:Double, precision:Double) =
		Math.round(value / precision) * precision

	fun isApproxEqual(valA: Double, valB: Double, e: Double = EPSILON) =
		Math.abs(valA - valB) < e

	fun isApproxEqual(round: Int, valA: Double, valB: Double) =
		Math.abs(valA - valB) < Math.pow(10.0, -round.toDouble())

	fun isApproxInteger(value: Double, e: Double = EPSILON) =
		isApproxEqual(e, value.roundToInt().toDouble(), value)

	fun intFloor(value: Double) =
		Math.floor(value).toInt()

	fun intFloor(value: BigDecimal) =
		intFloor(value.toDouble())

	fun intCeil(value: Double) =
		Math.ceil(value).toInt()

	fun intCeil(value: BigDecimal) =
		intCeil(value.toDouble())

	fun intRound(value: Double) =
		value.roundToInt()

	fun intRound(value: BigDecimal) =
		intRound(value.toDouble())

	fun avg(a: Double, b: Double) =
		(a + b) / 2

	fun clamp(value: Double, min: Double, max: Double): Double {
		AssertHelper.greaterThan(max, min)
		return when {
			value > max ->
				max
			value < min ->
				min
			else ->
				value
		}
	}

	/**
	 * min inclusive
	 * max exclusive
	 */
	fun cyclicClamp(value: Double, min: Double, max: Double): Double {
		if (min <= value && value < max) {
			return value
		}

		val delta = max - min
		val valueMinusMin = value - min
		val numDelta = Math.floor(valueMinusMin / delta)
		return valueMinusMin - numDelta * delta + min
	}

	fun cyclicAvg(min: Double, max: Double, firstValue: Double, vararg values: Double): Double {
		val delta = (max - min)
		val halfDelta = delta * 0.5
		var currAvg = MathHelper.cyclicClamp(firstValue, min, max)
		var i = 1
		values.forEach { v ->
			var clampedValue = MathHelper.cyclicClamp(v, min, max)
			if (Math.abs(v - currAvg) > halfDelta) {
				clampedValue += delta
			}
			currAvg = MathHelper.cyclicClamp((i * currAvg + clampedValue) / (i + 1.0), min, max)
			i++
		}

		return currAvg
	}

	/**
	 * Linear interpolate the value
	 */
	fun lerp01(value: Double, at0: Double, at1: Double) =
		when {
			value >= 1 ->
				at1
			value <= 0 ->
				at0
			else ->
				at0 + value * (at1 - at0)
		}

	fun assertClose(expected: Double, actual: Double, acceptedDelta: Double = 0.0001) {
		if (!MathHelper.isApproxEqual(expected, actual, acceptedDelta))
			throw AssertionFailedError("$expected not close enough to $actual, using delta=${acceptedDelta}", expected, actual)
	}

	fun assertNotClose(expected: Double, actual: Double, acceptedDelta: Double = 0.0001) {
		if (MathHelper.isApproxEqual(expected, actual, acceptedDelta))
			throw AssertionFailedError("$expected too close to $actual, using delta=${acceptedDelta}", expected, actual)
	}
}
