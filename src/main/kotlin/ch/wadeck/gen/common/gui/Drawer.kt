package ch.wadeck.gen.common.gui

import ch.wadeck.gen.common.util.geom.DoublePoint
import ch.wadeck.gen.common.util.geom.DoubleRect
import ch.wadeck.gen.common.util.geom.IntPoint
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Font
import java.awt.Graphics2D
import java.awt.font.TextLayout
import kotlin.math.absoluteValue


class Drawer(val g: Graphics2D, val scaleX: Double, val scaleY: Double) {
	fun color(color: Color) {
		g.color = color
	}
	fun background(color: Color) {
		g.background = color
	}

	fun stroke(stroke: Float) {
		g.stroke = BasicStroke(stroke)
	}

	fun drawLine(ax: Int, ay: Int, bx: Int, by: Int) {
		_drawLine(ax.toDouble(), ay.toDouble(), bx.toDouble(), by.toDouble())
	}

	fun drawLine(ax: Double, ay: Double, bx: Double, by: Double) {
		_drawLine(ax, ay, bx, by)
	}

	fun drawLine(a: IntPoint, b: IntPoint) {
		_drawLine(a.x.toDouble(), a.y.toDouble(), b.x.toDouble(), b.y.toDouble())
	}

	fun drawLine(a: DoublePoint, b: DoublePoint) {
		_drawLine(a.x, a.y, b.x, b.y)
	}

	private fun _drawLine(ax: Double, ay: Double, bx: Double, by: Double) {
		g.drawLine((ax * scaleX).toInt(), (ay * scaleY).toInt(), (bx * scaleX).toInt(), (by * scaleY).toInt())
	}

	fun drawRect(a: DoublePoint, b: DoublePoint) {
		_drawRect(a.x, a.y, b.x - a.x, b.y - a.y)
	}

	fun drawRect(r: DoubleRect) {
		_drawRect(r.minX, r.minY, r.maxX - r.minX, r.maxY - r.minY)
	}

	private fun _drawRect(x: Double, y: Double, w: Double, h: Double) {
		val _x: Double =
			if (scaleX < 0) {
				x + w
			} else {
				x
			}

		val _y: Double =
			if (scaleY < 0) {
				y + h
			} else {
				y
			}

		g.drawRect((_x * scaleX).toInt(), (_y * scaleY).toInt(), (w * scaleX).toInt().absoluteValue, (h * scaleY).toInt().absoluteValue)
	}

	fun fillRect(a: DoublePoint, b: DoublePoint) {
		_fillRect(a.x, a.y, b.x - a.x, b.y - a.y)
	}

	fun fillRect(r: DoubleRect) {
		_fillRect(r.minX, r.minY, r.maxX - r.minX, r.maxY - r.minY)
	}

	private fun _fillRect(x: Double, y: Double, w: Double, h: Double) {
		val _x: Double =
			if (scaleX < 0) {
				x + w
			} else {
				x
			}

		val _y: Double =
			if (scaleY < 0) {
				y + h
			} else {
				y
			}

		g.fillRect((_x * scaleX).toInt(), (_y * scaleY).toInt(), (w * scaleX).toInt().absoluteValue, (h * scaleY).toInt().absoluteValue)
	}

	fun drawDot(px: Int, py: Int, radius: Double = 0.5) {
		_drawOvalCentered(px.toDouble(), py.toDouble(), radius, radius)
	}

	fun drawDot(px: Double, py: Double, radius: Double = 0.5) {
		_drawOvalCentered(px, py, radius, radius)
	}

	fun drawDot(p: IntPoint, radius: Double = 0.5) {
		_drawOvalCentered(p.x.toDouble(), p.y.toDouble(), radius, radius)
	}

	fun drawDot(p: DoublePoint, radius: Double = 0.5) {
		_drawOvalCentered(p.x, p.y, radius, radius)
	}

	private fun _drawOvalCentered(cx: Double, cy: Double, rx: Double, ry: Double) {
		val rsx = rx * scaleX.absoluteValue
		val rsy = ry * scaleY.absoluteValue
		g.drawOval(((cx * scaleX - rsx)).toInt(), ((cy * scaleY - rsy)).toInt(), (rsx * 2).toInt(), (rsy * 2).toInt())
	}

	fun fillDot(p: IntPoint, radius: Double = 0.5) {
		_fillOvalCentered(p.x.toDouble(), p.y.toDouble(), radius, radius)
	}

	fun fillDot(p: DoublePoint, radius: Double = 0.5) {
		_fillOvalCentered(p.x, p.y, radius, radius)
	}

	private fun _fillOvalCentered(cx: Double, cy: Double, rx: Double, ry: Double) {
		val rsx = rx * scaleX.absoluteValue
		val rsy = ry * scaleY.absoluteValue
		g.fillOval(((cx * scaleX - rsx)).toInt(), ((cy * scaleY - rsy)).toInt(), (rsx * 2).toInt(), (rsy * 2).toInt())
	}

	fun drawTextCentered(px: Int, py: Int, message: String) {
		_drawStringCentered(message, px.toDouble(), py.toDouble())
	}

	fun drawTextCentered(px: Double, py: Double, message: String) {
		_drawStringCentered(message, px, py)
	}

	private fun _drawStringCentered(messageText: String, x: Double, y: Double, fontSize: Int = 14) {
		val context = g.fontRenderContext
		val messageTextFont = Font("Arial", Font.PLAIN, fontSize)
		val txt = TextLayout(messageText, messageTextFont, context)
		val bounds = txt.bounds
		val descent = txt.descent.toInt() // * scaleY
		val xString = scaleX * x - bounds.width / 2.0
		val yString = scaleY * y + (bounds.height + descent) / 2.0

		g.font = messageTextFont
		g.drawString(messageText, xString.toFloat(), yString.toFloat())
	}
}
