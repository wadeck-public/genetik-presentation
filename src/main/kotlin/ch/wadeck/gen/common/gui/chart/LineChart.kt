package ch.wadeck.gen.common.gui.chart

import ch.wadeck.gen.common.gui.Drawer
import ch.wadeck.gen.common.util.geom.DoublePoint
import java.awt.Color

class LineChart(var color: Color = Color.black, var stroke: Float = 0.5f) : Chart {
	val points = mutableListOf<DoublePoint>()

	override fun draw(d: Drawer) {
		if (points.size < 2) return

		d.color(color)
		d.stroke(stroke)

		repeat(points.size - 1) { i ->
			d.drawLine(points[i], points[i + 1])
		}
	}
}
