package ch.wadeck.gen.common.gui.chart

import ch.wadeck.gen.common.gui.Drawer

interface Chart {
	fun draw(d: Drawer)
}
