package ch.wadeck.gen.common.gui

import java.awt.BorderLayout
import javax.swing.*

class ChartFrame(title: String) {
	private val frame = JFrame()
	private val center = JPanel()
	private val bottom = JPanel()

	private val buttons = mutableListOf<Pair<String, () -> Unit>>()

	init {
		frame.title = title
		frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
		frame.layout = BorderLayout()

		center.layout = BorderLayout()
		frame.add(center, BorderLayout.CENTER)

		frame.add(bottom, BorderLayout.SOUTH)
	}

	fun addButton(name: String, onClick: () -> Unit) {
		buttons += Pair(name, onClick)
	}

	fun setMainComponent(panel: JPanel) {
		center.add(panel)
	}

	fun show() {
		buttons.forEach { (name, action) ->
			val button = JButton(name)
			button.addActionListener { action() }
			bottom.add(button)
		}

		frame.pack()
		frame.setLocationRelativeTo(null)

		SwingUtilities.invokeLater {
			frame.isVisible = true
		}
	}
}
