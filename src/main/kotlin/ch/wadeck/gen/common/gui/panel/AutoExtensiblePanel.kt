package ch.wadeck.gen.common.gui.panel

import ch.wadeck.gen.common.gui.Drawer
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.geom.DoublePoint
import ch.wadeck.gen.common.util.geom.DoubleRect
import ch.wadeck.gen.common.util.geom.IntPoint
import java.awt.Color
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.RenderingHints
import javax.swing.JPanel

abstract class AutoExtensiblePanel(var area: DoubleRect = DoubleRect.bottomLeft(0.0, 0.0, 1.0, 1.0)) : JPanel() {
	var centered = false

	fun expandIfRequired(p: IntPoint) {
		this.expandIfRequired(p.x.toDouble(), p.y.toDouble())
	}

	fun expandIfRequired(p: DoublePoint) {
		this.expandIfRequired(p.x, p.y)
	}

	fun expandIfRequired(px: Int, py: Int) {
		expandIfRequired(px.toDouble(), py.toDouble())
	}

	fun expandIfRequired(px: Double, py: Double) {
		if (area.minX > px || area.minY > py) {
			area = DoubleRect(Math.min(area.minX, px), Math.min(area.minY, py), area.maxX, area.maxY)
		}
		if (area.maxX < px || area.maxY < py) {
			area = DoubleRect(area.minX, area.minY, Math.max(area.maxX, px), Math.max(area.maxY, py))
		}
	}

	override final fun paintComponent(g: Graphics?) {
		if (g == null)
			return

		val panelWidth = size.width
		val panelHeight = size.height

		val g2 = g as Graphics2D
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)

		// reset background
		g2.color = Color.white
		g2.fillRect(0, 0, panelWidth, panelHeight)

		paint2dBeforeTransform(g2)

		if (centered) {
			// center to (0,0)
			g2.translate(panelWidth / 2, panelHeight / 2)
		} else {
			// center bottom left
			val ratioAboveZero =
				if(area.minY < 0){
					(area.height + area.minY)/area.height
				}else{
					1.0
				}
			g2.translate(0, MathHelper.intFloor(panelHeight * ratioAboveZero))
		}
//		g2.scale(1.0, -1.0)

		val scaleX = panelWidth / area.width
		val scaleY = -panelHeight / area.height

		paint2d(g2, scaleX, scaleY)

		val d = Drawer(g2, scaleX, scaleY)
		d.color(Color.black)
		draw(d)
	}

	open fun paint2dBeforeTransform(g: Graphics2D) {}
	open fun paint2d(g: Graphics2D, scaleX: Double, scaleY: Double) {}

	open fun draw(d: Drawer) {}
}
