package ch.wadeck.gen.common.gui.panel

import ch.wadeck.gen.common.gui.Drawer
import ch.wadeck.gen.experiment.tsp.CityMap
import java.awt.Color
import java.awt.Dimension

class CityMapPanel(val cities: CityMap) : AutoExtensiblePanel() {

	var result: IntArray? = null

	init {
		this.preferredSize = Dimension(400, 400)
		expandIfRequired(cities.boundary.maxX, cities.boundary.maxY)
//		cities.cities.forEach { city ->
//			expandIfRequired(city.x, city.y)
//		}
	}

	override fun draw(d: Drawer) {
//		d.color(Color.orange)
//		d.drawDot(0, 0, 50.0)
//
//		d.color(Color.red)
//		d.drawDot(0, 0, 100.0)

		val resultCopy = result?.toList()
		if (resultCopy != null) {
			repeat(resultCopy.size - 1) { i ->
				val coefToOne = (i / (resultCopy.size - 1.0f))
				d.color(Color(0.4f, 0.5f + (1 - coefToOne) * 0.4f, 0.4f))
				d.stroke(0.5f + (1 - coefToOne) * 2.5f)
				val from = cities.cities[resultCopy[i]]
				val to = cities.cities[resultCopy[i + 1]]
				d.drawLine(from.x, from.y, to.x, to.y)
			}
		}

		d.color(Color.blue)
		cities.cities.forEach { city ->
			d.drawDot(city.x, city.y, 4.0)
			d.drawTextCentered(city.x, city.y, city.name)
		}
	}
}
