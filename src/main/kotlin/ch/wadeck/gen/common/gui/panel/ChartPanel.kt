package ch.wadeck.gen.common.gui.panel

import ch.wadeck.gen.common.gui.Drawer
import ch.wadeck.gen.common.gui.chart.Chart
import ch.wadeck.gen.common.util.MathHelper
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Dimension
import java.awt.Graphics2D

open class ChartPanel : AutoExtensiblePanel() {
	//TODO put log scale capability

	val charts = mutableListOf<Chart>()

	init {
		this.preferredSize = Dimension(400, 400)
	}

	override fun paint2d(g: Graphics2D, scaleX: Double, scaleY: Double) {
		val horizontalUnit = getUnit(area.width)
		val verticalUnit = getUnit(area.height)

		val numHorizontalUnit = MathHelper.intCeil(area.width / horizontalUnit)
		val numVerticalUnit = MathHelper.intCeil(area.height / verticalUnit)

		if (centered) {
			g.color = Color.gray
			g.stroke = BasicStroke(0.5f)

			// horizontal axis
			g.drawLine((-area.width * scaleX).toInt(), 0, (area.width * scaleX).toInt(), 0)
			// vertical axis
			g.drawLine(0, (-area.height * scaleY).toInt(), 0, (area.height * scaleY).toInt())

			repeat(numHorizontalUnit) { i ->
				g.drawLine(
					(-i * horizontalUnit * scaleX).toInt(),
					-3,
					(-i * horizontalUnit * scaleX).toInt(),
					3
				)
				g.drawLine(
					(i * horizontalUnit * scaleX).toInt(),
					-3,
					(i * horizontalUnit * scaleX).toInt(),
					3
				)
			}
			repeat(numVerticalUnit) { i ->
				g.drawLine(
					-3,
					(-i * verticalUnit * scaleY).toInt(),
					3,
					(-i * verticalUnit * scaleY).toInt()
				)
				g.drawLine(
					-3,
					(i * verticalUnit * scaleY).toInt(),
					3,
					(i * verticalUnit * scaleY).toInt()
				)
			}
		} else {
			g.color = Color.gray
			g.stroke = BasicStroke(0.5f)
			g.drawLine((-area.width * scaleX).toInt(), 0, (area.width * scaleX).toInt(), 0)
			repeat(numHorizontalUnit) { i ->
				g.drawLine(
					(i * horizontalUnit * scaleX).toInt(),
					-5,
					(i * horizontalUnit * scaleX).toInt(),
					5
				)
			}
			repeat(numVerticalUnit) { i ->
				g.drawLine(
					1,
					(i * verticalUnit * scaleY).toInt(),
					5,
					(i * verticalUnit * scaleY).toInt()
				)
			}
		}
	}

	override fun draw(d: Drawer) {
		charts.forEach { c -> c.draw(d) }
	}

	companion object {
		val UNITS = intArrayOf(
			1, 2, 5, 10, 20, 50, 100, 200, 500,
			1000, 2000, 5000, 10_000, 20_000, 50_000,
			100_000, 200_000, 500_000, 1000_000, 2000_000, 5000_000,
			10_000_000, 20_000_000, 50_000_000
		)
	}

	private fun getUnit(value: Double) = UNITS.firstOrNull { u -> (value / u) < 20 } ?: UNITS.last()
}
