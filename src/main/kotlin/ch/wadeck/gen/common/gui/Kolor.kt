package ch.wadeck.gen.common.gui

import ch.wadeck.gen.common.util.MathHelper
import java.awt.Color

object Kolor {
	val LIGHT_RED = Color(255, 95, 95)
	val RED = Color(255, 0, 0)
	val DARK_RED = Color(120, 0, 0)

	val LIGHT_BLUE = Color(75, 75, 255)
	val BLUE = Color(0, 0, 255)
	val DARK_BLUE = Color(0, 0, 120)

	val LIGHT_GREEN = Color(125, 255, 125)
	val GREEN = Color(0, 210, 0)
	val DARK_GREEN = Color(0, 120, 0)

	val LIGHT_YELLOW = Color(255, 255, 150)
	val YELLOW = Color(255, 255, 0)
	val DARK_YELLOW = Color(180, 180, 0)

	// turquoise
	val LIGHT_CYAN = Color(160, 255, 255)
	val CYAN = Color(0, 255, 255)
	val DARK_CYAN = Color(0, 160, 160)

	// violet
	val LIGHT_MAGENTA = Color(255, 150, 255)
	val MAGENTA = Color(255, 0, 255)
	val DARK_MAGENTA = Color(180, 0, 180)

	val LIGHT_ORANGE = Color(255, 170, 100)
	val ORANGE = Color(255, 170, 0)
	val DARK_ORANGE = Color(190, 100, 0)

	val BLACK = Color(0, 0, 0)
	val VERY_DARK_GREY = Color(50, 50, 50)
	val DARK_GREY = Color(70, 70, 70)
	val GREY = Color(120, 120, 120)
	val LIGHT_GREY = Color(160, 160, 160)
	val WHITE = Color(255, 255, 255)
}

fun Color.withAlpha(coef: Double): Color =
	Color(this.red, this.green, this.blue, MathHelper.clamp(255 * coef, 0.0, 255.0).toInt())
