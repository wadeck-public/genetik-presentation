package ch.wadeck.gen.common.gui.panel

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.Source
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.gui.Kolor
import ch.wadeck.gen.common.gui.chart.LineChart
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.geom.DoublePoint
import java.awt.Color
import java.awt.Graphics2D

class QualityChartPanel(
	val displayAverage: Boolean = true,
	val displayWorst: Boolean = true
) : ChartPanel() {
	val bestScoreChart = LineChart(Color.green)
	val averageScoreChart = LineChart(Color.gray)
	val worstScoreChart = LineChart(Color.red)
	var currentGeneration = 1
	var bestScore = Double.NEGATIVE_INFINITY
	var lastBestImproveAtGenNo = 1

	init {
		charts += bestScoreChart
		if (displayAverage) {
			charts += averageScoreChart
		}
		if (displayWorst) {
			charts += worstScoreChart
		}
	}

	override fun paint2dBeforeTransform(g: Graphics2D) {
		g.color = Kolor.DARK_GREEN
		g.drawString("Green = best", 10, 10)
		if (displayAverage) {
			g.color = Kolor.DARK_GREY
			g.drawString("Grey = average", 10, 25)
		}
		if (displayWorst) {
			g.color = Kolor.DARK_RED
			g.drawString("Red = worst", 10, 40)
		}

		g.color = Kolor.BLACK
		g.drawString("Generation n° $currentGeneration", 100,  10)
		g.color = Kolor.DARK_GREY
		g.drawString("Best score: $bestScore", 100,  25)
		g.color = Kolor.DARK_GREY
		g.drawString("Best found at n° $lastBestImproveAtGenNo", 100, 40)
	}

	fun <M : Mutant<M>> updateAndRepaint(generation: MutantGeneration<M>) {
		currentGeneration = generation.generationNo
		lastBestImproveAtGenNo = generation.bestAtGenerationNo
		bestScore = MathHelper.approx(generation.bestMutant.fitnessScore)

		bestScoreChart.points += DoublePoint(generation.generationNo.toDouble(), generation.bestMutant.fitnessScore)
		expandIfRequired(bestScoreChart.points.last())
		if (!displayAverage && !displayWorst) {
			repaint()
			return
		}

		val nonRandomMutants = generation.mutants.filter { it.source != Source.RANDOM }

		if (displayAverage && generation.generationNo > 0) {
			if (nonRandomMutants.isNotEmpty()) {
				averageScoreChart.points += DoublePoint(
					generation.generationNo.toDouble(),
					nonRandomMutants.sumByDouble { it.fitnessScore } / nonRandomMutants.size
				)
			}
			if (averageScoreChart.points.isNotEmpty()) {
				expandIfRequired(averageScoreChart.points.last())
			}
		}

		if (displayWorst && generation.generationNo > 0) {
			// some random could be inside the "non-random" mutants if they are better, it's just an approximation
			val worstMutantBeforeRandom =
				if (nonRandomMutants.isEmpty()) {
					// first round everyone is random
					generation.mutants.last()
				} else {
					generation.mutants[nonRandomMutants.size - 1]
				}

			worstScoreChart.points += DoublePoint(generation.generationNo.toDouble(), worstMutantBeforeRandom.fitnessScore)

			expandIfRequired(worstScoreChart.points.last())
		}

		repaint()
	}
}
