package ch.wadeck.gen.common.generation.other.crossover

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.other.CrossoverBlock

class TakeJustOneParentBlock<M : Mutant<M>> : CrossoverBlock<M>() {
	override fun crossover(parentA: M, parentB: M): M {
		return parentA.copy()
	}
}
