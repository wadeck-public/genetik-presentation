package ch.wadeck.gen.common.generation.goal.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GoalBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.util.AssertHelper

class NoMoreImprovementRelativeGoalBlock<M : Mutant<M>>(
		private val percentOfNonImprovement: Double = 0.05,
		private val maxGeneration: Int = 500,
		private val subBlock: GoalBlock<M>
) : GoalBlock<M>() {
	private var noChangeCounter = 0

	init {
		AssertHelper.isStrictlyBetween01(percentOfNonImprovement)
		AssertHelper.isStrictlyPositive(maxGeneration)
	}

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subBlock.setup(generation)
	}

	override fun isGoalReached(): Boolean {
		val subGoalAchieved = subBlock.isGoalReached()
		if (subGoalAchieved) {
			noChangeCounter++
			val generation = Math.min(generation.generationNo, maxGeneration)
			val counterValueRequired = Math.max(2.0, Math.ceil(generation * percentOfNonImprovement)).toInt()
			if (counterValueRequired < noChangeCounter) {
				return true
			}
		} else {
			noChangeCounter = 0
		}

		return false
	}
}
