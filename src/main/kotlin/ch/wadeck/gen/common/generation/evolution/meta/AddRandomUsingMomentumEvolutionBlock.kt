package ch.wadeck.gen.common.generation.evolution.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.util.AssertHelper
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.RandomHelper

class AddRandomUsingMomentumEvolutionBlock<M : Mutant<M>>(
	private val percentLow: Double = 0.05,
	private val percentHigh: Double = 0.50,
	private val subBlock: EvolutionBlock<M>
) : EvolutionBlock<M>() {
	init {
		AssertHelper.isBetween01(percentLow)
		AssertHelper.isBetween01(percentHigh)
	}

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subBlock.setup(generation)
	}

	override fun evolves(numToGenerate: Int): List<M> {
		val stability = 1 - generation.momentum
		val s3 = Math.pow(stability, 3.0)
		val percentOfRandom = MathHelper.lerp01(s3, percentLow, percentHigh)

		val numToGenerateRandomly = RandomHelper.randomRound(percentOfRandom * numToGenerate, generation.random)

		val newRandoms = List(numToGenerateRandomly) {
			generation.mutantGeneratorBlock.doGenerateNew()
		}

		val newGeneration = subBlock.doEvolves(numToGenerate - numToGenerateRandomly)

		return newRandoms + newGeneration
	}
}
