package ch.wadeck.gen.common.generation.goal

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GoalBlock
import ch.wadeck.gen.common.util.MathHelper

class BestNotChangingGoalBlock<M : Mutant<M>> : GoalBlock<M>() {
	private var lastBest = Double.MIN_VALUE

	override fun isGoalReached(): Boolean {
		val currentBest = generation.mutants.first().fitnessScore
		if (MathHelper.isApproxEqual(6, currentBest, lastBest)) {
			lastBest = currentBest
			return true
		}

		lastBest = currentBest
		return false
	}
}