package ch.wadeck.gen.common.generation.evolution.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.util.AssertHelper
import ch.wadeck.gen.common.util.RandomHelper

class AddSomeRandomEvolutionBlock<M : Mutant<M>>(
	private val percentOfRandomAdded: Double = 0.05,
	private val subBlock: EvolutionBlock<M>
) : EvolutionBlock<M>() {
	init {
		AssertHelper.isBetween01(percentOfRandomAdded)
	}

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subBlock.setup(generation)
	}

	override fun evolves(numToGenerate: Int): List<M> {
		val numToGenerateRandomly = RandomHelper.randomRound(percentOfRandomAdded * numToGenerate, generation.random)

		val newRandoms = List(numToGenerateRandomly){
			generation.mutantGeneratorBlock.doGenerateNew()
		}

		val newGeneration = subBlock.doEvolves(numToGenerate - numToGenerateRandomly)

		return newRandoms + newGeneration
	}
}
