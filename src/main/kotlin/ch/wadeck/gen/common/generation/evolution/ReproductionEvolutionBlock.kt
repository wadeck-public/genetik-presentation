package ch.wadeck.gen.common.generation.evolution

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.common.generation.other.SelectionBlock

class ReproductionEvolutionBlock<M : Mutant<M>>(
	private val subSelectionBlock: SelectionBlock<M>,
	private val subCrossoverBlock: CrossoverBlock<M>
) : EvolutionBlock<M>() {
	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subSelectionBlock.setup(generation)
		this.subCrossoverBlock.setup(generation)
	}

	override fun evolves(numToGenerate: Int): List<M> =
		List(numToGenerate) {
			val parentA = subSelectionBlock.selectNextParent()
			val parentB = subSelectionBlock.selectNextParent()
			subCrossoverBlock.doCrossover(parentA, parentB)
		}
}
