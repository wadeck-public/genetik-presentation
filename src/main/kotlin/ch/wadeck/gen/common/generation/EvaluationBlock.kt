package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant

@JvmSuppressWildcards
abstract class EvaluationBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * Fill the lastResult on each mutant, higher is better
	 */
	abstract fun computeFitness(mutants:List<M>)
}
