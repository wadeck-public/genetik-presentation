package ch.wadeck.gen.common.generation.other

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GenerationBlock

abstract class SelectionBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * Select a parent in the current generation for the reproduction
	 */
	abstract fun selectNextParent(): M
}