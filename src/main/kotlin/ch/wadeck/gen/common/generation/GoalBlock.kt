package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant

abstract class GoalBlock<M : Mutant<M>> : GenerationBlock<M>() {
	abstract fun isGoalReached(): Boolean
}
