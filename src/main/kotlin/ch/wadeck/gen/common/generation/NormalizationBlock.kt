package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant

abstract class NormalizationBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * After this call all the mutants must have their normalizedScore computed and between 0 and 1
	 * The distribution is dependant of the implementation
	 */
	abstract fun normalize()
}
