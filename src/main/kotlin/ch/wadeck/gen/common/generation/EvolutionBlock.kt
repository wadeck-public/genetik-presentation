package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.util.AssertHelper

abstract class EvolutionBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * Make the current generation to evolve into the next one
	 * Only new mutant are allowed (at least a copy of the existing)
	 * with their lastResult null
	 */
	protected abstract fun evolves(numToGenerate:Int):List<M>

	fun doEvolves(numToGenerate:Int):List<M> {
		AssertHelper.isPositive(numToGenerate)

		if(numToGenerate == 0){
			return listOf()
		}

		return evolves(numToGenerate)
	}
}
