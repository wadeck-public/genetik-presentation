package ch.wadeck.gen.common.generation.other.crossover.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.common.util.Stats

class BestOfTwoCrossoverBlock<M : Mutant<M>>(
	private val aCrossoverBlock: CrossoverBlock<M>,
	private val bCrossoverBlock: CrossoverBlock<M>
) : CrossoverBlock<M>() {

	private val statsA: Stats = Stats()
	private val statsB: Stats = Stats()
	private val statsCommon: Stats = Stats()

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.aCrossoverBlock.setup(generation)
		this.bCrossoverBlock.setup(generation)
	}

	override fun crossover(parentA: M, parentB: M): M {
		val ma = aCrossoverBlock.doCrossover(parentA, parentB)
		val mb = bCrossoverBlock.doCrossover(parentA, parentB)

		generation.evaluationBlock.computeFitness(listOf(ma, mb))

		statsA += ma.fitnessScore
		statsB += mb.fitnessScore

		var best: M
		if (ma.fitnessScore > mb.fitnessScore) {
			best = ma
			statsCommon += 50
		} else {
			best = mb
			statsCommon += -50
		}

		ma.resetScore()
		mb.resetScore()

		return best
	}
}