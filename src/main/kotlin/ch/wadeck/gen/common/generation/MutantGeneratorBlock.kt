package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.Source

abstract class MutantGeneratorBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * Create a new mutant
	 */
	protected abstract fun generateNew(): M

	fun doGenerateNew() =
		generateNew().apply { this.source = Source.RANDOM }
}
