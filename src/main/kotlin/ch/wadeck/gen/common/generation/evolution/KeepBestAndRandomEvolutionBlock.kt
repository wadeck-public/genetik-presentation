package ch.wadeck.gen.common.generation.evolution

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.Source
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.util.AssertHelper
import ch.wadeck.gen.common.util.RandomHelper

class KeepBestAndRandomEvolutionBlock<M : Mutant<M>>(
	private val percentToKeep: Double = 0.2
) : EvolutionBlock<M>() {
	init {
		AssertHelper.isBetween01(percentToKeep)
	}

	override fun evolves(numToGenerate: Int): List<M> {
		val numberToKeep = RandomHelper.randomRound(percentToKeep * numToGenerate, generation.random)
		val numberToGenerateRandomly = numToGenerate - numberToKeep

		val best: List<M> = generation.mutants.subList(0, numberToKeep).map { m ->
			m.copy().apply {
				this.source = Source.COPY
				this.parentId = m.id
			}
		}
		val random: List<M> = List(numberToGenerateRandomly){
			generation.mutantGeneratorBlock.doGenerateNew()
		}

		return best + random
	}
}
