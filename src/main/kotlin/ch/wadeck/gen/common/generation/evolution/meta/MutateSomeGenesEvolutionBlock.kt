package ch.wadeck.gen.common.generation.evolution.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.other.SelfMutationBlock

class MutateSomeGenesEvolutionBlock<M : Mutant<M>>(
	private val selfMutationBlock: SelfMutationBlock<M>,
	private val subBlock: EvolutionBlock<M>
) : EvolutionBlock<M>() {
	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.selfMutationBlock.setup(generation)
		this.subBlock.setup(generation)
	}

	override fun evolves(numToGenerate: Int): List<M> {
		val newGeneration = subBlock.doEvolves(numToGenerate)
		val newGenerationMutated = newGeneration.map { m ->
			selfMutationBlock.doSelfMutation(m)
		}

		return newGenerationMutated
	}
}
