package ch.wadeck.gen.common.generation.other.normalization

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.NormalizationBlock

/**
 * For a distributed set of number, 10% of the scores will be >0.5
 */
class LogNormalizationBlock<M : Mutant<M>> : NormalizationBlock<M>() {
	override fun normalize() {
		var bestScore = generation.mutants.first().fitnessScore
		var worstScore = generation.mutants.last().fitnessScore
		val delta = bestScore - worstScore
		val bestTarget = - delta / 100.0

		val linearAdjust = bestScore - bestTarget
		bestScore = bestTarget
		worstScore -= linearAdjust

		val max: Double = -Math.log10(-bestScore)
		val min: Double = -Math.log10(-worstScore)

		generation.mutants.forEach {
			it.normalizedScore = (-Math.log10(-(it.fitnessScore - linearAdjust)) - min) / (max - min)
		}
	}
}
