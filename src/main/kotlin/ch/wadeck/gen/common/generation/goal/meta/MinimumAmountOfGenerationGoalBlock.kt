package ch.wadeck.gen.common.generation.goal.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GoalBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.util.AssertHelper

class MinimumAmountOfGenerationGoalBlock<M : Mutant<M>>(
		private val minimumNumberOfGeneration: Int = 10,
		private val subBlock: GoalBlock<M>
) : GoalBlock<M>() {
	init {
		AssertHelper.isStrictlyPositive(minimumNumberOfGeneration)
	}

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subBlock.setup(generation)
	}

	override fun isGoalReached(): Boolean {
		if (minimumNumberOfGeneration >= generation.generationNo) {
			return false
		}

		return subBlock.isGoalReached()
	}
}