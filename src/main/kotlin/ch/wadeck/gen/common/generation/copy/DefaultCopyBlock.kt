package ch.wadeck.gen.common.generation.copy

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.CopyBlock

class DefaultCopyBlock<M : Mutant<M>> : CopyBlock<M>() {
	override fun copy(mutant: M) = mutant.copy()
}