package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant

abstract class GenerationBlock<M : Mutant<M>> {
	lateinit var generation: MutantGeneration<M>
		protected set

	open fun setup(generation: MutantGeneration<M>) {
		this.generation = generation
	}
}