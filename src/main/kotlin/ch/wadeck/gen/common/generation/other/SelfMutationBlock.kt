package ch.wadeck.gen.common.generation.other

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.Source
import ch.wadeck.gen.common.generation.GenerationBlock

abstract class SelfMutationBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * Self mutate the mutant by tweaking its genes. In case there is not change, just return the mutant itself.
	 */
	protected abstract fun selfMutation(mutant: M): M

	fun doSelfMutation(mutant: M) = selfMutation(mutant).apply {
		if(this == mutant){
			// in case there was no mutation just return it
			return@apply
		}

		this.source = Source.MUTATION
		if (mutant.normalizedScore == Mutant.NORMALIZED_SCORE_NOT_SET) {
			// we are inside a generation, the initial mutant was already a child
			// we steal its parent to have only "one generation" per generation
			this.parentId = mutant.parentId
			this.secondParentId = mutant.secondParentId
		} else {
			this.parentId = mutant.id
		}
	}
}
