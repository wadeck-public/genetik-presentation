package ch.wadeck.gen.common.generation.other.crossover.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.common.util.Stats

class BestOfCrossoverBlock<M : Mutant<M>>(
	private val crossoverBlockList: List<CrossoverBlock<M>>
) : CrossoverBlock<M>() {

	private val statsList: List<Stats> = List(crossoverBlockList.size){ Stats() }

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.crossoverBlockList.forEach{ it.setup(generation) }
	}

	override fun crossover(parentA: M, parentB: M): M {
		val ms = crossoverBlockList.map { it.doCrossover(parentA, parentB)}

		generation.evaluationBlock.computeFitness(ms)

		ms.forEachIndexed{ index, m ->
			statsList[index] += m.fitnessScore
		}

		val best: M = ms.maxBy { it.fitnessScore } ?: ms[0]

		ms.forEach{ it.resetScore() }

		return best
	}
}
