package ch.wadeck.gen.common.generation.evolution.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.Source
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.other.SelfMutationBlock
import ch.wadeck.gen.common.util.MathHelper

class KeepTheBestUsingMomentumEvolutionBlock<M : Mutant<M>>(
	private val percentLow: Double = 0.0,
	private val percentHigh: Double = 0.3,
	private val threshold: Double = 0.5,
	private val subSelfMutationBlock: SelfMutationBlock<M>,
	private val subEvolutionBlock: EvolutionBlock<M>
) : EvolutionBlock<M>() {
	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subEvolutionBlock.setup(generation)
		this.subSelfMutationBlock.setup(generation)
	}

	override fun evolves(numToGenerate: Int): List<M> {
		val currentBest = generation.bestMutant
		val bestClone = currentBest.copy().apply {
			this.source = Source.COPY
			this.parentId = currentBest.id
		}

		val stability = 1 - generation.momentum
		val s3 = Math.pow(stability, 3.0)
		val percentOfBestSelfMutation =
			if (s3 > threshold) {
				MathHelper.lerp01((s3 - threshold)/ threshold, percentLow, percentHigh)
			} else {
				0.0
			}

		val numOfBestSelfMutation = MathHelper.intRound(generation.mutantPerGeneration * percentOfBestSelfMutation)
		val bestSelfMutation = List(numOfBestSelfMutation) {
			subSelfMutationBlock.doSelfMutation(currentBest.copy()).apply {
				this.parentId = currentBest.id
			}
		}

		val newGeneration = subEvolutionBlock.doEvolves(numToGenerate - numOfBestSelfMutation - 1)

		return bestSelfMutation + newGeneration.plus(bestClone)
	}
}
