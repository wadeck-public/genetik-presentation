package ch.wadeck.gen.common.generation.goal

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GoalBlock
import ch.wadeck.gen.common.util.AssertHelper

class MajorityOfMutantInSameRangeGoalBlock<M : Mutant<M>>(
		private val percentOfBestMutantToLookAt: Double,
		relativeDifferenceWithBest: Double
) : GoalBlock<M>() {

	private val scoreToReach: Double

	init {
		AssertHelper.isStrictlyBetween01(percentOfBestMutantToLookAt)
		AssertHelper.isStrictlyBetween01(relativeDifferenceWithBest)

		scoreToReach = 1 - relativeDifferenceWithBest
	}

	override fun isGoalReached(): Boolean {
		// as the normalizedScore range is [0, 1] we know that the best score is 1
		val lastIndex = Math.max(1, ((percentOfBestMutantToLookAt * generation.mutants.size) - 1).toInt())
		val worseMutantToLookAt = generation.mutants[lastIndex]
		val worseScore = worseMutantToLookAt.normalizedScore

		return worseScore >= scoreToReach
	}
}