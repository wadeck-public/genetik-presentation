package ch.wadeck.gen.common.generation.goal.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GoalBlock
import ch.wadeck.gen.common.generation.MutantGeneration

class AndAllGoalBlock<M : Mutant<M>>(
	vararg val subBlockList: GoalBlock<M>
) : GoalBlock<M>() {
	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subBlockList.forEach { it.setup(generation) }
	}

	override fun isGoalReached(): Boolean {
		return subBlockList.all { it.isGoalReached() }
	}
}
