package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant

abstract class StabilizationBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * Called after generation fitness computation
	 * @return momentum in (0, 1), 0 meaning the solution is not moving at all, 1 meaning there was recently a huge advance in terms of fitness increase
	 */
	internal abstract fun computeMomentum(): Double
}
