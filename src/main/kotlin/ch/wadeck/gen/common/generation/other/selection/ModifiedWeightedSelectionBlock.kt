package ch.wadeck.gen.common.generation.other.selection

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.other.SelectionBlock
import ch.wadeck.gen.common.util.RandomHelper

class ModifiedWeightedSelectionBlock<M : Mutant<M>>(
	private val weightModifier: (Double) -> Double
) : SelectionBlock<M>() {
	private var cacheForGenNo: Int = -1
	private var weightedEntries: List<Pair<Double, M>> = emptyList()

	override fun selectNextParent(): M {
		ensureCacheReady()

		return RandomHelper.pickWeightedSingleRandomFrom(weightedEntries, generation.random)
	}

	private fun ensureCacheReady() {
		if (generation.generationNo == cacheForGenNo) {
			return
		}

		cacheForGenNo = generation.generationNo
		weightedEntries = generation.mutants.map { m -> Pair(weightModifier(m.normalizedScore), m) }
	}
}
