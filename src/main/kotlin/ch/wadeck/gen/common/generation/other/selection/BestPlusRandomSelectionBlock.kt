package ch.wadeck.gen.common.generation.other.selection

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.other.SelectionBlock
import ch.wadeck.gen.common.util.AssertHelper
import ch.wadeck.gen.common.util.RandomHelper

class BestPlusRandomSelectionBlock<M : Mutant<M>>(
	private val percentChanceForBest: Double,
	private val percentOfBest: Double
) : SelectionBlock<M>() {

	init {
		AssertHelper.isBetween01(percentChanceForBest)
		AssertHelper.isBetween01(percentOfBest)
	}

	override fun selectNextParent(): M {
		val numOfBest = RandomHelper.randomRound(generation.mutantPerGeneration * percentOfBest, generation.random)

		val subList =
			if (generation.random.nextFloat() < percentChanceForBest) {
				generation.mutants.subList(0, numOfBest)
			} else {
				generation.mutants.subList(numOfBest, generation.mutants.size)
			}

		return RandomHelper.pickSingleRandomFrom(subList, generation.random)
	}
}
