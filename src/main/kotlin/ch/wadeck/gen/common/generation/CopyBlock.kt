package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.Source

abstract class CopyBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * Use the mutateGenes with a coef of 0 to make a copy
	 */
	protected abstract fun copy(mutant: M): M

	fun doCopy(mutant: M) = copy(mutant).apply {
		this.source = Source.COPY
		this.parentId = mutant.id
	}
}
