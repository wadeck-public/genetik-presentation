package ch.wadeck.gen.common.generation.other

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.Source
import ch.wadeck.gen.common.generation.GenerationBlock

abstract class CrossoverBlock<M : Mutant<M>> : GenerationBlock<M>() {
	/**
	 * Create a new mutant by reproduction with the two parents
	 */
	protected abstract fun crossover(parentA: M, parentB: M): M

	fun doCrossover(parentA: M, parentB: M): M {
		val result = crossover(parentA, parentB).apply {
			this.source = Source.BREED
			this.parentId = parentA.id
			this.secondParentId = parentB.id
		}

		return result
	}
}