package ch.wadeck.gen.common.generation.evolution.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.generation.MutantGeneration

class CheckDuplicateEvolutionBlock<M : Mutant<M>>(
	private val subBlock: EvolutionBlock<M>
) : EvolutionBlock<M>() {
	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subBlock.setup(generation)
	}

	override fun evolves(numToGenerate: Int): List<M> {
		val newGeneration = subBlock.doEvolves(numToGenerate)
		val pathMap = mutableMapOf<String, Int>()
		newGeneration.forEach { m ->
			val key = m.getGeneAsString()
			pathMap.compute(key, { _, u -> u?.inc() ?: 1 })
		}

		val duplicates = pathMap.filter { e -> e.value >= 2 }
		if (duplicates.isNotEmpty()) {
			println("duplicates: ${generation.generationNo}: ${duplicates}")
		}

		return newGeneration
	}
}
