package ch.wadeck.gen.common.generation.stabilization

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.StabilizationBlock
import ch.wadeck.gen.common.util.AssertHelper

class LastScoreStabilizationBlock<M : Mutant<M>>(
	val depreciationFactor:Double
): StabilizationBlock<M>() {
	init{
		AssertHelper.isBetween01(depreciationFactor)
	}

	var lastBestScore:Double = Double.NEGATIVE_INFINITY
	var currentMomentum:Double = 1.0

	override fun computeMomentum(): Double {
		val currentBestScore = generation.mutants[0].fitnessScore
		if(currentBestScore > lastBestScore){
			lastBestScore = currentBestScore
			currentMomentum = 1.0
		}else{
			currentMomentum *= depreciationFactor
		}

		return currentMomentum
	}
}
