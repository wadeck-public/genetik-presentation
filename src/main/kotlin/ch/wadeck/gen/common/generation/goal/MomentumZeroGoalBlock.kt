package ch.wadeck.gen.common.generation.goal

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GoalBlock

class MomentumZeroGoalBlock<M : Mutant<M>> : GoalBlock<M>() {
	private var momentumThreshold = 1 * Math.pow(0.9, 50.0)

	override fun isGoalReached(): Boolean {
		return generation.momentum < momentumThreshold
	}
}
