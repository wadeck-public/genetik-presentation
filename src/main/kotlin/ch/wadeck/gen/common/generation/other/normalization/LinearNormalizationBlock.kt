package ch.wadeck.gen.common.generation.other.normalization

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.NormalizationBlock

/**
 * Normalize using simple linear modification.
 *
 * The value distribution will stay the same
 */
class LinearNormalizationBlock<M : Mutant<M>> : NormalizationBlock<M>() {
	override fun normalize() {
		val max:Double = generation.mutants.first().fitnessScore
		val min:Double = generation.mutants.last().fitnessScore

		generation.mutants.forEach{
			it.normalizedScore = (it.fitnessScore - min) / (max - min)
		}
	}
}
