package ch.wadeck.gen.common.generation.evolution

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.EvolutionBlock

class PureRandomEvolutionBlock<M : Mutant<M>> : EvolutionBlock<M>() {
	override fun evolves(numToGenerate: Int) = List(numToGenerate) {
		generation.mutantGeneratorBlock.doGenerateNew()
	}
}
