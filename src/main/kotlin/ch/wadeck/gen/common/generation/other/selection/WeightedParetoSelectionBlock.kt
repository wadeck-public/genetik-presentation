package ch.wadeck.gen.common.generation.other.selection

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.other.SelectionBlock
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.RandomHelper

class WeightedParetoSelectionBlock<M : Mutant<M>> : SelectionBlock<M>() {
	private var num20Percent: Int = -1
	private var cacheForGenNo: Int = -1
	private var weighted20Entries: List<Pair<Double, M>> = emptyList()
	private var weighted80Entries: List<Pair<Double, M>> = emptyList()

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		num20Percent = MathHelper.intRound(generation.mutantPerGeneration * 0.2)
	}

	override fun selectNextParent(): M {
		ensureCacheReady()

		return if (generation.random.nextDouble() < 0.8) {
			RandomHelper.pickWeightedSingleRandomFrom(weighted20Entries, generation.random)
		} else {
			RandomHelper.pickWeightedSingleRandomFrom(weighted80Entries, generation.random)
		}
	}

	private fun ensureCacheReady() {
		if (generation.generationNo == cacheForGenNo) {
			return
		}

		cacheForGenNo = generation.generationNo
		weighted20Entries = generation.mutants.subList(0, num20Percent)
			.map { m -> Pair(m.normalizedScore, m) }
		weighted80Entries = generation.mutants.subList(num20Percent, generation.mutants.size)
			.map { m -> Pair(m.normalizedScore, m) }
	}
}
