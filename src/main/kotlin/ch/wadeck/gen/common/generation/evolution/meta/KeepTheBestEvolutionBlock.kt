package ch.wadeck.gen.common.generation.evolution.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.Source
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.generation.MutantGeneration

class KeepTheBestEvolutionBlock<M : Mutant<M>>(
	private val subBlock: EvolutionBlock<M>
) : EvolutionBlock<M>() {
	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subBlock.setup(generation)
	}

	override fun evolves(numToGenerate: Int): List<M> {
		val bestClone = generation.mutants[0].copy().apply {
			this.source = Source.COPY
			this.parentId = generation.mutants[0].id
		}

		val newGeneration = subBlock.doEvolves(numToGenerate - 1)

		return newGeneration.plus(bestClone)
	}
}
