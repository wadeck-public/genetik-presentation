package ch.wadeck.gen.common.generation.goal.meta

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GoalBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.util.AssertHelper

class NoMoreImprovementAbsoluteGoalBlock<M : Mutant<M>>(
		private val numberOfGeneration: Int = 100,
		private val subBlock: GoalBlock<M>
) : GoalBlock<M>() {
	private var noChangeCounter = 0

	init {
		AssertHelper.isStrictlyPositive(numberOfGeneration)
	}

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subBlock.setup(generation)
	}

	override fun isGoalReached(): Boolean {
		val subGoalAchieved = subBlock.isGoalReached()
		if (subGoalAchieved) {
			noChangeCounter++
			if (numberOfGeneration <= noChangeCounter) {
				return true
			}
		} else {
			noChangeCounter = 0
		}

		return false
	}
}
