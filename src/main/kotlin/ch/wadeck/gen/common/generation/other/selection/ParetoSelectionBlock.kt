package ch.wadeck.gen.common.generation.other.selection

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.other.SelectionBlock
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.RandomHelper

class ParetoSelectionBlock<M : Mutant<M>> : SelectionBlock<M>() {
	override fun selectNextParent(): M {
		val limit = MathHelper.intRound(generation.mutantPerGeneration * 0.2)
		return if (generation.random.nextDouble() < 0.8) {
			RandomHelper.pickSingleRandomFrom(generation.mutants.subList(0, limit), generation.random)
		} else {
			RandomHelper.pickSingleRandomFrom(generation.mutants.subList(limit, generation.mutantPerGeneration), generation.random)
		}
	}
}
