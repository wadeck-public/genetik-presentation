package ch.wadeck.gen.common.generation.goal

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.GoalBlock

class NeverOkBlock<M : Mutant<M>> : GoalBlock<M>() {
	override fun isGoalReached(): Boolean  = false
}
