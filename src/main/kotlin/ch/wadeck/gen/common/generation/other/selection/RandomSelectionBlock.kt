package ch.wadeck.gen.common.generation.other.selection

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.other.SelectionBlock
import ch.wadeck.gen.common.util.RandomHelper

class RandomSelectionBlock<M : Mutant<M>> : SelectionBlock<M>() {
	override fun selectNextParent() =
		RandomHelper.pickSingleRandomFrom(generation.mutants, generation.random)
}
