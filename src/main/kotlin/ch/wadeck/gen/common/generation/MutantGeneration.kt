package ch.wadeck.gen.common.generation

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.copy.DefaultCopyBlock
import ch.wadeck.gen.common.generation.other.normalization.LogNormalizationBlock
import ch.wadeck.gen.common.generation.stabilization.LastScoreStabilizationBlock
import ch.wadeck.gen.common.util.AssertHelper
import java.util.*

class MutantGeneration<M : Mutant<M>>(
	val mutantPerGeneration: Int,
	val random: Random,
	val mutantGeneratorBlock: MutantGeneratorBlock<M>,
	val evaluationBlock: EvaluationBlock<M>,
	val goalBlock: GoalBlock<M>,
	val evolutionBlock: EvolutionBlock<M>,
	val copyBlock: CopyBlock<M> = DefaultCopyBlock(),
	val normalizationBlock: NormalizationBlock<M> = LogNormalizationBlock(),
	val stabilizationBlock: StabilizationBlock<M> = LastScoreStabilizationBlock(0.9)
) {
	var generationNo = 0
	var bestAtGenerationNo = 0
	var previousBest = Double.NEGATIVE_INFINITY

	var mutants: List<M>
	var momentum: Double = 1.0

	var lastBestMutantId: Int = NOT_SET
	lateinit var bestMutant: M

	init {
		AssertHelper.isStrictlyPositive(mutantPerGeneration)

		mutantGeneratorBlock.setup(this)
		evaluationBlock.setup(this)
		goalBlock.setup(this)
		evolutionBlock.setup(this)
		copyBlock.setup(this)
		normalizationBlock.setup(this)
		stabilizationBlock.setup(this)

		mutants = List(mutantPerGeneration) {
			mutantGeneratorBlock.doGenerateNew()
		}
	}

	fun nextGeneration() {
		val newGeneration: List<M> = evolutionBlock.doEvolves(mutantPerGeneration)
		AssertHelper.isEqual(newGeneration.size, mutantPerGeneration)

		this.mutants = newGeneration
		generationNo++
	}

	fun computeFitness(): Boolean {
		// will fill the fitnessScore of each mutants
		evaluationBlock.computeFitness(mutants)

		mutants = mutants.sortedBy { -it.fitnessScore }
		lastBestMutantId = if (generationNo > 1) bestMutant.id else NOT_SET
		bestMutant = mutants.first()

		normalizationBlock.normalize()

		momentum = stabilizationBlock.computeMomentum()

		val currentBestScore = bestMutant.fitnessScore
		if(previousBest < currentBestScore){
			previousBest = currentBestScore
			bestAtGenerationNo = generationNo
		}

		return goalBlock.isGoalReached()
	}

	companion object {
		const val NOT_SET = -1
	}
}
