package ch.wadeck.gen.common.generation.evolution

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.generation.EvolutionBlock
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.common.generation.other.SelectionBlock

class ReproductionBetweenDifferentEvolutionBlock<M : Mutant<M>>(
	private val subSelectionBlock: SelectionBlock<M>,
	private val subCrossoverBlock: CrossoverBlock<M>
) : EvolutionBlock<M>() {
	val maxRetries = 10

	override fun setup(generation: MutantGeneration<M>) {
		super.setup(generation)
		this.subSelectionBlock.setup(generation)
		this.subCrossoverBlock.setup(generation)
	}

	override fun evolves(numToGenerate: Int): List<M> =
		List(numToGenerate) {
			val parentA = subSelectionBlock.selectNextParent()
			val parentAGenes = parentA.getGeneAsString()
			var i = maxRetries
			var parentB = subSelectionBlock.selectNextParent()
			while (parentB.getGeneAsString() == parentAGenes && i > 0) {
				parentB = subSelectionBlock.selectNextParent()
				i--
			}
			subCrossoverBlock.doCrossover(parentA, parentB)
		}
}
