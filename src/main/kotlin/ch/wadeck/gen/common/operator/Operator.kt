package ch.wadeck.gen.common.operator

import ch.wadeck.gen.common.Copyable

sealed class Operator : Copyable<Operator> {
	abstract fun compute(context: Context): Double

	fun traverse(currentDepth: Int = 0, onOperator: (Operator, Int) -> Unit): Unit =
		when (this) {
			is NoneOperator ->
				onOperator(this, currentDepth)
			is MonoOperator -> {
				onOperator(this, currentDepth)
				this.op.traverse(currentDepth + 1, onOperator)
			}
			is DualOperator -> {
				onOperator(this, currentDepth)
				this.op1.traverse(currentDepth + 1, onOperator)
				this.op2.traverse(currentDepth + 1, onOperator)
			}
		}

	/**
	 * Traverse from the root to the leaves
	 */
	fun traverseAndCopy(currentDepth: Int = 0, createCopyOrNull: (current: Operator, depth: Int) -> Operator?): Operator =
		when (this) {
			is NoneOperator -> {
				val newOp = createCopyOrNull(this, currentDepth)
				newOp ?: this.copy()
			}
			is MonoOperator -> {
				val newOp = createCopyOrNull(this, currentDepth)
				if (newOp != null) {
					newOp
				} else {
					val child = this.op.traverseAndCopy(currentDepth + 1, createCopyOrNull)
					this.copyUsing(child)
				}
			}
			is DualOperator -> {
				val newOp = createCopyOrNull(this, currentDepth)
				if (newOp != null) {
					newOp
				} else {
					val child1 = this.op1.traverseAndCopy(currentDepth + 1, createCopyOrNull)
					val child2 = this.op2.traverseAndCopy(currentDepth + 1, createCopyOrNull)
					this.copyUsing(child1, child2)
				}
			}
		}

	/**
	 * Traverse from the leaves to the root. Each node is copied then passed to the closure.
	 */
	fun traverseBackwardAndCopy(currentDepth: Int = 0, createCopyOrNull: (current: Operator, newCurrent: Operator, Int) -> Operator?): Operator =
		when (this) {
			is NoneOperator -> {
				val newCurrent = this.copy()
				createCopyOrNull(this, newCurrent, currentDepth) ?: newCurrent
			}
			is MonoOperator -> {
				val child = this.op.traverseBackwardAndCopy(currentDepth + 1, createCopyOrNull)
				val newCurrent = this.copyUsing(child)
				createCopyOrNull(this, newCurrent, currentDepth) ?: newCurrent
			}
			is DualOperator -> {
				val child1 = this.op1.traverseBackwardAndCopy(currentDepth + 1, createCopyOrNull)
				val child2 = this.op2.traverseBackwardAndCopy(currentDepth + 1, createCopyOrNull)
				val newCurrent = this.copyUsing(child1, child2)
				createCopyOrNull(this, newCurrent, currentDepth) ?: newCurrent
			}
		}

	abstract override fun toString(): String
	abstract fun isSameAs(that: Operator, precision: Double): Boolean
}

/**
 * @see ch.wadeck.gen.experiment.operator.impl.Constant
 * @see ch.wadeck.gen.experiment.operator.impl.Variable
 */
abstract class NoneOperator : Operator()

/**
 *
 */
abstract class MonoOperator(val op: Operator) : Operator() {
	/**
	 * Both parameter must be a fresh copy
	 */
	abstract fun copyUsing(newOp: Operator): MonoOperator
}

abstract class DualOperator(val op1: Operator, val op2: Operator) : Operator() {
	/**
	 * Both parameter must be a fresh copy
	 */
	abstract fun copyUsing(newOp1: Operator, newOp2: Operator): DualOperator
}
