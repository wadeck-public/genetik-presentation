package ch.wadeck.gen.common.operator

enum class Type {
	// monomial
	VARIABLE_NAME_NOT_EQUAL,
	COEF_INVALID_DOUBLE,
	COEF_ONLY_INVALID_DOUBLE,
	EXPONANT_INVALID_DOUBLE,
	EXPONANT_ONLY_INVALID_DOUBLE,
	// polynomial
	DUAL_OPERATION_UNSUPPORTED,
	NONE_OPERATION_UNSUPPORTED
}

class XnomialParsingException(val type: Type, msg: String) : RuntimeException(msg)
