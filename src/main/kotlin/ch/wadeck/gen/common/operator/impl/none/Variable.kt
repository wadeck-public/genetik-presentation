package ch.wadeck.gen.common.operator.impl.none

import ch.wadeck.gen.common.operator.Context
import ch.wadeck.gen.common.operator.NoneOperator
import ch.wadeck.gen.common.operator.Operator
import ch.wadeck.gen.common.util.AssertHelper

class Variable(val name: String) : NoneOperator() {
	override fun compute(context: Context): Double {
		val value = context.map[name]
		AssertHelper.isNotNull(value)

		return value!!
	}

	override fun copy(): Operator =
		Variable(name)

	override fun toString(): String =
		name

	override fun isSameAs(that: Operator, precision: Double) =
		that is Variable && this.name == that.name
}
