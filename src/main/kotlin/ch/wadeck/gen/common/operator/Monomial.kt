package ch.wadeck.gen.common.operator

import ch.wadeck.gen.common.Copyable
import ch.wadeck.gen.common.operator.Type.VARIABLE_NAME_NOT_EQUAL
import ch.wadeck.gen.common.util.MathHelper

const val TIMES_SIGN = '·'

class Monomial(val coef: Double = 1.0, variables: List<Pair<String, Int>>) : Comparable<Monomial>, Copyable<Monomial> {
	// could be x, a_b_c or x2_y3
	val variablesName: String
	val variables: List<Pair<String, Int>>

	init {
		this.variables = variables.sortedWith(Comparator { p1, p2 ->
			val lower1 = p1.first.toLowerCase()
			val lower2 = p2.first.toLowerCase()
			if (lower1 == lower2) {
				p1.first.compareTo(p2.first)
			} else {
				lower1.compareTo(lower2)
			}
		})

		this.variablesName = variables.joinToString("_") {
			if (it.second == 1) {
				it.first
			} else {
				"${it.first}${it.second}"
			}
		}
	}

	operator fun plus(that: Monomial) =
		if (this.variablesName != that.variablesName) {
			throw XnomialParsingException(
				VARIABLE_NAME_NOT_EQUAL,
				"Two monomials with different variables cannot be summed, ${this.variablesName} != ${that.variablesName}"
			)
		} else {
			Monomial(this.coef + that.coef, this.variables)
		}

	operator fun times(that: Monomial) =
		Monomial(this.coef * that.coef, sumVariable(this.variables + that.variables))

	private fun sumVariable(variables: List<Pair<String, Int>>): List<Pair<String, Int>> {
		val sumVariables = mutableListOf<Pair<String, Int>>()
		variables.groupBy { pair -> pair.first }.forEach { keyToPairList ->
			val totalExponant = keyToPairList.value.sumBy { it.second }
			sumVariables += Pair(keyToPairList.key, totalExponant)
		}

		return sumVariables
	}

	override fun copy(): Monomial =
		Monomial(this.coef, this.variables.map { it.copy() })

	override fun equals(other: Any?): Boolean {
		if (this === other)
			return true
		if (javaClass != other?.javaClass)
			return false

		other as Monomial

		if (!MathHelper.isApproxEqual(coef, other.coef))
			return false

		if (variablesName != other.variablesName)
			return false

		return true
	}

	override fun hashCode(): Int {
		var result = MathHelper.approx(coef).hashCode()
		result = 31 * result + variablesName.hashCode()
		return result
	}

	override fun compareTo(other: Monomial): Int {
		val totalDegreeThis = this.variables.sumBy { it.second }
		val totalDegreeThat = other.variables.sumBy { it.second }
		if (totalDegreeThis != totalDegreeThat) {
			return totalDegreeThat.compareTo(totalDegreeThis)
		}

		val flattenVariableNamesThis = flattenPairListUsingExponent(this.variables)
		val flattenVariableNamesThat = flattenPairListUsingExponent(other.variables)

		return flattenVariableNamesThis.joinToString("").compareTo(flattenVariableNamesThat.joinToString(""))
	}

	private fun flattenPairListUsingExponent(varList: List<Pair<String, Int>>): List<String> {
		val result = mutableListOf<String>()

		varList.forEach { pair ->
			repeat(pair.second) {
				result += pair.first
			}
		}

		return result
	}


	fun isNegative() =
		coef < 0

	fun hasCoef() =
		!MathHelper.isApproxEqual(coef, 1.0)

	override fun toString(): String {
		val approxCoef: Number =
			if (MathHelper.isApproxInteger(coef)) {
				MathHelper.intRound(coef)
			} else {
				MathHelper.approx(coef)
			}

		val coefDiffFromZero = !MathHelper.isApproxEqual(coef, 0.0)
		if (!coefDiffFromZero) {
			return "0"
		}

		return if (this.variablesName.isNotBlank()) {
			if (hasCoef()) {
				approxCoef.toString() + TIMES_SIGN + "" + variablesName.replace('_', TIMES_SIGN)
			} else {
				variablesName.replace('_', TIMES_SIGN)
			}
		} else {
			approxCoef.toString()
		}
	}

	fun toStringWithoutSign():String {
		val withSign = toString()
		return if (withSign[0] == '-') {
			withSign.substring(1)
		} else {
			withSign
		}
	}

	companion object {
		fun normalize(monomials: List<Monomial>): List<Monomial> =
			monomials
				.groupBy { it.variablesName }
				.map { entry ->
					val coefSum = entry.value.sumByDouble { it.coef }
					Monomial(coefSum, entry.value[0].variables)
				}
				.filter { !MathHelper.isApproxEqual(it.coef, 0.0) }
				.sorted()
	}
}
