package ch.wadeck.gen.common.operator

data class VariableNames(val keySet: Set<String>){
	fun getAllNames() =
		keySet.toList()
}
