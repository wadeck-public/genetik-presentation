package ch.wadeck.gen.common.operator

import ch.wadeck.gen.common.operator.impl.dual.Addition
import ch.wadeck.gen.common.operator.impl.dual.Multiplication
import ch.wadeck.gen.common.operator.impl.mono.Inverse
import ch.wadeck.gen.common.operator.impl.mono.Opposite
import ch.wadeck.gen.common.operator.impl.none.Constant
import ch.wadeck.gen.common.operator.impl.none.Variable

operator fun Operator.plus(that: String) = this + Variable(that)
operator fun Operator.plus(that: Int) = this + Constant(that.toDouble())
operator fun Operator.plus(that: Double) = this + Constant(that)
operator fun Operator.plus(that: Operator) = Addition(this, that)

operator fun Operator.times(that: String) = this * Variable(that)
operator fun Operator.times(that: Int) = this * Constant(that.toDouble())
operator fun Operator.times(that: Double) = this * Constant(that)
operator fun Operator.times(that: Operator) = Multiplication(this, that)

operator fun Operator.unaryMinus() = Opposite(this)
fun Operator.inv() = Inverse(this)

fun Double.op() = Constant(this)
fun Int.op() = Constant(this.toDouble())
fun String.op() = Variable(this)
