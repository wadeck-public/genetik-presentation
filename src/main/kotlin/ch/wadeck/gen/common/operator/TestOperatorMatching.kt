package ch.wadeck.gen.common.operator

import ch.wadeck.gen.common.operator.impl.none.Constant
import ch.wadeck.gen.common.operator.impl.none.Variable

class TestOperatorMatching {
	fun test(op: Operator, context: Context) = when (op) {
		is NoneOperator ->
			0.0
		is MonoOperator ->
			op.compute(context)
		is DualOperator ->
			op.op1.compute(context)
	}

	fun test2() {
		val t1 = Variable("c") + Constant(23.0)
		val t2 = Variable("c") + Constant(23.0)
		val t3 = t1 * t2
		val t6 = t1 + "c"
		val t7 = ("c".op() + 5.op()) + t6 * t3
		val t8 = (t1 + t2) * t6
	}
}
