package ch.wadeck.gen.common.operator.impl.mono

import ch.wadeck.gen.common.operator.Context
import ch.wadeck.gen.common.operator.MonoOperator
import ch.wadeck.gen.common.operator.Operator

class Inverse(op: Operator) : MonoOperator(op) {
	override fun compute(context: Context): Double =
		1.0 / op.compute(context)

	override fun copyUsing(newOp: Operator): MonoOperator =
		Inverse(newOp)

	override fun copy(): Operator =
		Inverse(op.copy())

	override fun toString(): String =
		"inv($op)"

	override fun isSameAs(that: Operator, precision: Double) =
		that is Inverse && this.op.isSameAs(that.op, precision)
}
