package ch.wadeck.gen.common.operator

import ch.wadeck.gen.common.operator.Type.DUAL_OPERATION_UNSUPPORTED
import ch.wadeck.gen.common.operator.Type.NONE_OPERATION_UNSUPPORTED
import ch.wadeck.gen.common.operator.impl.dual.Addition
import ch.wadeck.gen.common.operator.impl.dual.Multiplication
import ch.wadeck.gen.common.operator.impl.none.Constant
import ch.wadeck.gen.common.operator.impl.none.Variable

class Polynomial(monomials: List<Monomial>) {
	val monomials: List<Monomial> = Monomial.normalize(monomials)
	val variableToMonomials = this.monomials.associateBy { it.variablesName }

	operator fun plus(that: Polynomial): Polynomial {
		var result = Polynomial.empty()

		val remainingInThat = that.variableToMonomials.toMutableMap()
		this.variableToMonomials.forEach { variableName, currentMonomial ->
			val removedMonomial = remainingInThat.remove(variableName)
			if (removedMonomial == null) {
				result += currentMonomial
			} else {
				result += currentMonomial + removedMonomial
			}
		}

		remainingInThat.values.forEach { mono ->
			result += mono
		}

		return result
	}

	operator fun plus(monomial: Monomial): Polynomial {
		val newMonomialList =
			if (variableToMonomials.containsKey(monomial.variablesName)) {
				this.monomials.map {
					if (it.variablesName == monomial.variablesName) {
						it + monomial
					} else {
						it.copy()
					}
				}
			} else {
				this.monomials.map { it.copy() } + monomial
			}

		return Polynomial(newMonomialList)
	}

	operator fun times(that: Polynomial): Polynomial {
		var result = Polynomial.empty()

		this.monomials.forEach { monoFromThis ->
			that.monomials.forEach { monoFromThat ->
				result += monoFromThis * monoFromThat
			}
		}

		return result
	}

	override fun toString(): String {
		if (monomials.isEmpty()) {
			return ""
		}

		val sb = StringBuilder()
		if (monomials[0].isNegative()) {
			sb.append('-')
		}

		val s = monomials.size - 1
		repeat(s) { i ->
			sb.append(monomials[i].toStringWithoutSign())
			if (monomials[i + 1].isNegative()) {
				sb.append(" - ")
			} else {
				sb.append(" + ")
			}
		}
		sb.append(monomials.last().toStringWithoutSign())

		return sb.toString()
	}

	companion object {
		fun empty() =
			Polynomial(emptyList())

		fun buildFromOperator(operator: Operator): Polynomial =
			Polynomial.fromOperation(operator)

		private fun fromOperation(operator: Operator): Polynomial =
			if (operator is DualOperator) {
				val op1 = operator.op1
				val op2 = operator.op2

				val poly1 = fromOperation(op1)
				val poly2 = fromOperation(op2)
				when (operator) {
					is Addition ->
						poly1 + poly2
					is Multiplication ->
						poly1 * poly2
					else ->
						throw XnomialParsingException(
							DUAL_OPERATION_UNSUPPORTED,
							"Unsupported class for fromOperation(${operator.javaClass} ~ $operator)"
						)
				}
			} else {
				when (operator) {
					is Constant ->
						Polynomial(listOf(Monomial(operator.fixedValue, emptyList())))
					is Variable ->
						Polynomial(listOf(Monomial(1.0, listOf(Pair(operator.name, 1)))))
					else ->
						throw XnomialParsingException(
							NONE_OPERATION_UNSUPPORTED,
							"Unsupported class for fromOperation(${operator.javaClass} ~ $operator)"
						)
				}
			}
	}
}
