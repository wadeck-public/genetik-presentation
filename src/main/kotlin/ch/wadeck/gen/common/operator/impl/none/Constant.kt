package ch.wadeck.gen.common.operator.impl.none

import ch.wadeck.gen.common.operator.Context
import ch.wadeck.gen.common.operator.NoneOperator
import ch.wadeck.gen.common.operator.Operator
import ch.wadeck.gen.common.util.MathHelper

class Constant(val fixedValue: Double) : NoneOperator() {
	constructor(fixedValue: Int) : this(fixedValue.toDouble())

	override fun compute(context: Context): Double =
		fixedValue

	override fun copy(): Operator =
		Constant(fixedValue)

	override fun toString(): String =
		"${MathHelper.approx(fixedValue)}"

	override fun isSameAs(that: Operator, precision: Double) =
		that is Constant && MathHelper.isApproxEqual(this.fixedValue, that.fixedValue, precision)
}
