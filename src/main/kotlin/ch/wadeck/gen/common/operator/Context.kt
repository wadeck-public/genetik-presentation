package ch.wadeck.gen.common.operator

data class Context(val map: Map<String, Double>) {
	fun getVariableNames(): VariableNames = VariableNames(this.map.keys)
}
