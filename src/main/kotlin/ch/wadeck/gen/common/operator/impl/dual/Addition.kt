package ch.wadeck.gen.common.operator.impl.dual

import ch.wadeck.gen.common.operator.Context
import ch.wadeck.gen.common.operator.DualOperator
import ch.wadeck.gen.common.operator.Operator

class Addition(op1: Operator, op2: Operator) : DualOperator(op1, op2) {
	override fun compute(context: Context): Double =
		op1.compute(context) + op2.compute(context)

	override fun copyUsing(newOp1: Operator, newOp2: Operator): DualOperator =
		Addition(newOp1, newOp2)

	override fun copy(): Operator =
		Addition(op1.copy(), op2.copy())

	override fun toString(): String =
		"($op1 + $op2)"

	override fun isSameAs(that: Operator, precision: Double) =
		that is Addition && (
			(this.op1.isSameAs(that.op1, precision) && this.op2.isSameAs(that.op2, precision)) ||
				(this.op1.isSameAs(that.op2, precision) && this.op2.isSameAs(that.op1, precision))
			)
}
