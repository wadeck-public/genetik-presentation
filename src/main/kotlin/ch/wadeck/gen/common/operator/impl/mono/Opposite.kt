package ch.wadeck.gen.common.operator.impl.mono

import ch.wadeck.gen.common.operator.Context
import ch.wadeck.gen.common.operator.MonoOperator
import ch.wadeck.gen.common.operator.Operator

class Opposite(op: Operator) : MonoOperator(op) {
	override fun compute(context: Context): Double =
		-op.compute(context)

	override fun copyUsing(newOp: Operator): MonoOperator =
		Opposite(newOp)

	override fun copy(): Operator =
		Opposite(op.copy())

	override fun toString(): String =
		"opp($op)"

	// not accurate
	override fun isSameAs(that: Operator, precision: Double) =
		that is Opposite && this.op.isSameAs(that.op, precision)
}
