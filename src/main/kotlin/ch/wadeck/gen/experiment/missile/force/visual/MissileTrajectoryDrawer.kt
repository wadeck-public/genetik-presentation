package ch.wadeck.gen.experiment.missile.force.visual

import ch.wadeck.gen.common.gui.Drawer
import ch.wadeck.gen.common.gui.chart.Chart
import ch.wadeck.gen.experiment.missile.common.MissileLevel
import ch.wadeck.gen.experiment.missile.force.MissileForceMutant
import java.awt.Color

class MissileTrajectoryDrawer(
	var missileLevel: MissileLevel,
	var color: Color = Color.black,
	var stroke: Float = 0.5f,
	var withDots: Boolean = false
) : Chart {
	var missiles:List<MissileForceMutant> = emptyList()

	override fun draw(d: Drawer) {
		d.color(color)
		d.stroke(stroke)

		missiles.forEach { m ->
			if(withDots)
				d.drawDot(missileLevel.start)
			if(m.positions.isEmpty()){
				return@forEach
			}

			d.drawLine(missileLevel.start, m.positions[0])

			repeat(m.positions.size - 1){ i ->
				if(withDots)
					d.drawDot(m.positions[i])
				d.drawLine(m.positions[i], m.positions[i+ 1])
			}
			d.fillDot(m.positions.last())
		}
	}
}
