package ch.wadeck.gen.experiment.missile.force

import ch.wadeck.gen.common.Source
import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.evolution.ReproductionBetweenDifferentEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.ReproductionEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.AddRandomUsingMomentumEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.KeepTheBestEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.KeepTheBestUsingMomentumEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.MutateSomeGenesEvolutionBlock
import ch.wadeck.gen.common.generation.goal.NeverOkBlock
import ch.wadeck.gen.common.generation.other.selection.WeightedParetoSelectionBlock
import ch.wadeck.gen.common.gui.ChartFrame
import ch.wadeck.gen.common.gui.Kolor
import ch.wadeck.gen.common.gui.panel.ChartPanel
import ch.wadeck.gen.common.gui.panel.QualityChartPanel
import ch.wadeck.gen.common.gui.withAlpha
import ch.wadeck.gen.common.util.geom.DoublePoint
import ch.wadeck.gen.common.util.geom.DoubleRect
import ch.wadeck.gen.experiment.missile.common.MissileLevel
import ch.wadeck.gen.experiment.missile.common.MissileLevelDrawer
import ch.wadeck.gen.experiment.missile.force.block.MissileForceEvaluationBlock
import ch.wadeck.gen.experiment.missile.force.block.MissileForceMutantGeneratorBlock
import ch.wadeck.gen.experiment.missile.force.block.MissileForceSelfMutationBlock
import ch.wadeck.gen.experiment.missile.force.block.crossover.MissileForceMutantMixingCrossoverBlock
import ch.wadeck.gen.experiment.missile.force.block.crossover.MissileForceMutantSingleCutCrossoverBlock
import ch.wadeck.gen.experiment.missile.force.visual.MissileTrajectoryDrawer
import java.awt.GridLayout
import java.util.*
import javax.swing.JPanel

class Missile_01_Simple {
	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			Missile_01_Simple()
				.start()
		}
	}

	val missileLevel: MissileLevel

	val MUTANT_PER_GENERATION = 100
	//	val MUTANT_PER_GENERATION = 20
	val random: Random

	val generation: MutantGeneration<MissileForceMutant>

	val frame: ChartFrame
	val NEW_RANDOM_RATIO_PER_GENERATION = 0.02

	val updateDrawing: () -> Unit

	init /* prepare data */ {
		val mainSeed = 100000L
		val seedRandom = Random(mainSeed)

		random = Random(seedRandom.nextLong())

		missileLevel = MissileLevel(
			DoublePoint(50.0, 5.0),
			DoublePoint(50.0, 95.0),
			DoubleRect(0.0, 0.0, 100.0, 100.0),
			arrayListOf(
				DoubleRect(10.0, 40.0, 70.0, 60.0)
			)
		)
//		missileLevel = MissileLevel(
//			DoublePoint(50.0, 5.0),
//			DoublePoint(50.0, 95.0),
//			DoubleRect(0.0, 0.0, 100.0, 100.0),
//			arrayListOf(
//				DoubleRect(10.0, 40.0, 70.0, 60.0)
//			)
//		)

//		missileLevel = MissileLevel(
//			DoublePoint(50.0, 5.0),
//			DoublePoint(50.0, 95.0),
//			DoubleRect(0.0, 0.0, 100.0, 100.0),
//			arrayListOf(
//				DoubleRect(0.0, 30.0, 70.0, 40.0),
//				DoubleRect(30.0, 60.0, 100.0, 70.0)
//			)
//		)
	}

	init /* prepare generation */ {
		val mutantGenerator = MissileForceMutantGeneratorBlock(50, 0.3)

		val evaluationBlock = MissileForceEvaluationBlock(missileLevel)

		val goalBlock = NeverOkBlock<MissileForceMutant>()

		val evolutionBlock =
			//CheckDuplicateEvolutionBlock(
			KeepTheBestUsingMomentumEvolutionBlock(
				0.0, 0.3,
				0.7,
				// more modification but with less magnitude
				MissileForceSelfMutationBlock(0.2, 0.3),
//				AddSomeRandomEvolutionBlock(NEW_RANDOM_RATIO_PER_GENERATION,
				AddRandomUsingMomentumEvolutionBlock(0.02, 0.4,
					MutateSomeGenesEvolutionBlock(
						MissileForceSelfMutationBlock(0.1, 0.3),
						ReproductionBetweenDifferentEvolutionBlock(
//						ReproductionEvolutionBlock(
//							WeightedSelectionBlock(),
//							ModifiedWeightedSelectionBlock({ x -> x * x * x }),
							WeightedParetoSelectionBlock(),
//							ModifiedWeightedParetoSelectionBlock({ x -> x * x * x }),
//							MissileForceMutantSingleCutCrossoverBlock()
							MissileForceMutantMixingCrossoverBlock()
						)
					)
				)
			)
//		)

		val evolutionBlock2 =
			//CheckDuplicateEvolutionBlock(
			KeepTheBestEvolutionBlock(
//				AddSomeRandomEvolutionBlock(NEW_RANDOM_RATIO_PER_GENERATION,
				AddRandomUsingMomentumEvolutionBlock(0.02, 0.4,
					MutateSomeGenesEvolutionBlock(
						MissileForceSelfMutationBlock(0.1, 0.3),
//						ReproductionBetweenDifferentEvolutionBlock(
						ReproductionEvolutionBlock(
//							WeightedSelectionBlock(),
//							ModifiedWeightedSelectionBlock({ x -> x * x * x }),
							WeightedParetoSelectionBlock(),
//							ModifiedWeightedParetoSelectionBlock({ x -> x * x * x }),
							MissileForceMutantSingleCutCrossoverBlock()
						)
					)
				)
			)
//		)

		generation = MutantGeneration(
			MUTANT_PER_GENERATION,
			random,
			mutantGenerator,
			evaluationBlock,
			goalBlock,
			evolutionBlock
		)
	}

	init /* prepare view */ {
		val qualityPanel = QualityChartPanel()

		val missilePanel = ChartPanel()
		val levelDrawer = MissileLevelDrawer(missileLevel)

		val theBestMissile = MissileTrajectoryDrawer(missileLevel, Kolor.DARK_GREEN, withDots = true)
		val childrenOfBestMissile = MissileTrajectoryDrawer(missileLevel, Kolor.GREEN.withAlpha(0.9))
		val mutationOfBestMissile = MissileTrajectoryDrawer(missileLevel, Kolor.LIGHT_MAGENTA.withAlpha(0.9))
		val breededMissile = MissileTrajectoryDrawer(missileLevel, Kolor.LIGHT_ORANGE.withAlpha(0.7))
		val pureRandomMissiles = MissileTrajectoryDrawer(missileLevel, Kolor.GREY.withAlpha(0.5))

		missilePanel.charts += levelDrawer
		missilePanel.charts += pureRandomMissiles
		missilePanel.charts += breededMissile
		missilePanel.charts += mutationOfBestMissile
		missilePanel.charts += childrenOfBestMissile
		missilePanel.charts += theBestMissile
		missilePanel.area = missileLevel.area

		updateDrawing = {
			qualityPanel.updateAndRepaint(generation)
			updateVisualizationChart(
				theBestMissile, childrenOfBestMissile,
				mutationOfBestMissile, breededMissile,
				pureRandomMissiles
			)
			missilePanel.repaint()
		}

		frame = ChartFrame("Missile 01 - Simple")

		val panel = JPanel(GridLayout())
		panel.add(qualityPanel)
		panel.add(missilePanel)
		frame.setMainComponent(panel)

		frame.addButton("Next") {
			generation.nextGeneration()
			generation.computeFitness()
			updateDrawing()
			printStep()
		}
		frame.addButton("Next 10") {
			repeat(10) {
				generation.nextGeneration()
				generation.computeFitness()
				updateDrawing()
				printStep()
			}
		}
		frame.addButton("Direct 10") {
			repeat(10) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			updateDrawing()
			printStep()
		}
		frame.addButton("Next 50") {
			repeat(50) {
				generation.nextGeneration()
				generation.computeFitness()
				updateDrawing()
				printStep()
			}
		}
		frame.addButton("Direct 50") {
			repeat(50) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			updateDrawing()
			printStep()
		}
		frame.addButton("Direct 500") {
			val start = System.currentTimeMillis()
			repeat(500) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			val end = System.currentTimeMillis()
			updateDrawing()
			printStep()
			println("${end - start}ms")
		}
		frame.addButton("Direct 10000") {
			val start = System.currentTimeMillis()
			repeat(10000) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			val end = System.currentTimeMillis()
			updateDrawing()
			printStep()
			println("${end - start}ms")
		}
	}

	private fun updateVisualizationChart(theBestMissile: MissileTrajectoryDrawer,
	                                     childrenOfBestMissile: MissileTrajectoryDrawer,
	                                     mutationOfBestMissile: MissileTrajectoryDrawer,
	                                     breededMissile: MissileTrajectoryDrawer,
	                                     pureRandomMissiles: MissileTrajectoryDrawer
	) {
		val childrenOfBest = mutableListOf<MissileForceMutant>()
		val mutationOfBest = mutableListOf<MissileForceMutant>()
		val breeded = mutableListOf<MissileForceMutant>()
		val pureRandom = mutableListOf<MissileForceMutant>()

		generation.mutants.subList(1, generation.mutantPerGeneration).forEach { m ->
			if (m.source == Source.RANDOM) {
				pureRandom += m
			} else {
				if (m.parentId == generation.lastBestMutantId) {
					if(m.secondParentId == null && m.source != Source.COPY){
						mutationOfBest += m
					}else{
						childrenOfBest += m
					}
				} else {
					breeded += m
				}
			}
		}

		theBestMissile.missiles = generation.mutants.subList(0, 1)
		childrenOfBestMissile.missiles = childrenOfBest
		mutationOfBestMissile.missiles = mutationOfBest
		breededMissile.missiles = breeded
		pureRandomMissiles.missiles = pureRandom
	}

	private fun printStep() {
		val best = generation.mutants.first()
//		println("\t${generation.generationNo}, best: score=${best.fitnessScore}, normalized=${best.toSimplifiedString()}, func=${best.toExtendedString()}")
		println("\t${generation.generationNo}, best: score=${best.fitnessScore}, extract=${best.toSimplifiedString()}}")
	}

	fun start() {
		generation.computeFitness()
		printStep()
		updateDrawing()
		frame.show()
	}
}

