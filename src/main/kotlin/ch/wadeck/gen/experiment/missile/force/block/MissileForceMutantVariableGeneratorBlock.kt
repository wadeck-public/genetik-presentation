package ch.wadeck.gen.experiment.missile.force.block

import ch.wadeck.gen.common.generation.MutantGeneratorBlock
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.experiment.missile.force.MissileForceMutant

class MissileForceMutantVariableGeneratorBlock(
	val numOfAngle: Int,
	val numOfAngleVariance: Double,
	val force: Double,
	val forceVariance:Double
) : MutantGeneratorBlock<MissileForceMutant>() {
	override fun generateNew(): MissileForceMutant {
		val numOfAngleCurr = MathHelper.intRound(numOfAngle + (1 + generation.random.nextGaussian() * forceVariance))
		val angles = (1..numOfAngleCurr).map { generation.random.nextDouble() }
		return MissileForceMutant(angles, force * (1 + generation.random.nextGaussian() * forceVariance))
	}
}
