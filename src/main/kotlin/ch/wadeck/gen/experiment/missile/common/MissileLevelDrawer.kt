package ch.wadeck.gen.experiment.missile.common

import ch.wadeck.gen.common.gui.Drawer
import ch.wadeck.gen.common.gui.chart.Chart
import ch.wadeck.gen.experiment.missile.force.MissileForceMutant
import java.awt.Color

class MissileLevelDrawer(
	var missileLevel: MissileLevel,
	var borderColor: Color = Color.darkGray,
	var obstacleColor: Color = Color.gray,
	var startColor: Color = Color.blue,
	val goalColor: Color = Color.green,
	var boarderStroke: Float = 5f,
	var obstacleStroke: Float = 2f,
	var stroke: Float = 2f
) : Chart {
	var missiles: List<MissileForceMutant> = emptyList()

	override fun draw(d: Drawer) {
		d.stroke(boarderStroke)
		d.color(borderColor)

		d.drawLine(missileLevel.area.minX, missileLevel.area.minY, missileLevel.area.maxX, missileLevel.area.minY)
		d.drawRect(missileLevel.area)

		d.stroke(obstacleStroke)
		d.color(obstacleColor)
		missileLevel.obstacles.forEach {
			d.drawRect(it)
		}

		d.stroke(stroke)
		d.color(startColor)
		d.drawDot(missileLevel.start, 1.0)

		d.color(goalColor)
		d.drawDot(missileLevel.goal, 1.0)
	}
}
