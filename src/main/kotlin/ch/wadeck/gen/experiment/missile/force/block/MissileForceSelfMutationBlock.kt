package ch.wadeck.gen.experiment.missile.force.block

import ch.wadeck.gen.common.generation.other.SelfMutationBlock
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.experiment.missile.force.MissileForceMutant

class MissileForceSelfMutationBlock(
	val coefPerGene: Double,
	val coefAngleModification: Double
) : SelfMutationBlock<MissileForceMutant>() {
	override fun selfMutation(mutant: MissileForceMutant): MissileForceMutant {
		var hasMutated = false
		val allAngles = mutant.angles.toMutableList().map {
			if (generation.random.nextDouble() < coefPerGene) {
				val newAngle = it + generation.random.nextGaussian() * coefAngleModification
				hasMutated = true
				MathHelper.cyclicClamp(newAngle, 0.0, 1.0)
			} else {
				it
			}
		}

		return if (hasMutated) {
			MissileForceMutant(allAngles, mutant.force)
		} else {
			mutant
		}
	}
}

