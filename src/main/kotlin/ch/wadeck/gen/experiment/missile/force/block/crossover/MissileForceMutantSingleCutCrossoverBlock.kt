package ch.wadeck.gen.experiment.missile.force.block.crossover

import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.experiment.missile.force.MissileForceMutant

class MissileForceMutantSingleCutCrossoverBlock : CrossoverBlock<MissileForceMutant>() {
	override fun crossover(parentA: MissileForceMutant, parentB: MissileForceMutant): MissileForceMutant {
		val pa: List<Double>
		val pb: List<Double>
		val remaining: List<Double>
		when {
			parentA.angles.size > parentB.angles.size -> {
				pa = parentA.angles.subList(0, parentB.angles.size)
				pb = parentB.angles
				remaining = parentA.angles.subList(parentB.angles.size, parentA.angles.size)
			}
			parentA.angles.size < parentB.angles.size -> {
				pa = parentA.angles
				pb = parentB.angles.subList(0, parentA.angles.size)
				remaining = emptyList()
			}
			else -> {
				pa = parentA.angles
				pb = parentB.angles
				remaining = emptyList()
			}
		}

		val cutStart = 1 + generation.random.nextInt(pa.size - 3)
		val cutLength = 1 + generation.random.nextInt(pa.size - cutStart - 1)

		return MissileForceMutant(
			pa.subList(0, cutStart) +
				pb.subList(cutStart, cutStart + cutLength) +
				pa.subList(cutStart + cutLength, pa.size) + remaining,
			MathHelper.avg(parentA.force, parentB.force)
		)
	}
}
