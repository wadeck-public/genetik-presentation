package ch.wadeck.gen.experiment.missile.force.block

import ch.wadeck.gen.common.generation.EvaluationBlock
import ch.wadeck.gen.common.util.GeoHelper
import ch.wadeck.gen.common.util.geom.DoublePoint
import ch.wadeck.gen.experiment.missile.common.MissileLevel
import ch.wadeck.gen.experiment.missile.force.MissileForceMutant

class MissileForceEvaluationBlock(
	val missileLevel: MissileLevel,
	val stepTime: Double = 1.0,
	val goalPrecision: Double = 1.0
) : EvaluationBlock<MissileForceMutant>() {
	val goalPrecisionSqr = goalPrecision * goalPrecision

	override fun computeFitness(mutants: List<MissileForceMutant>) {
		mutants.forEach { m ->
			simulate(m)
		}
	}

	private fun simulate(m: MissileForceMutant) {
		var vx = 0.0
		var vy = 0.0

		val positions = mutableListOf<DoublePoint>()

		var goalReached = false

		var hasCrashed = false
		var smallestDistance = Double.MAX_VALUE
		var currPosition = missileLevel.start
		var prevPosition: DoublePoint
		m.angles.any { angle ->
			val angleWithPi = angle * 2 * Math.PI - Math.PI
			prevPosition = currPosition
			vx += stepTime * m.force * Math.cos(angleWithPi)
			vy += stepTime * m.force * Math.sin(angleWithPi)
			currPosition = DoublePoint(currPosition.x + vx * stepTime, currPosition.y + vy * stepTime)
			positions += currPosition

			val distanceToGoal = GeoHelper.squaredDistance(currPosition, missileLevel.goal)
			if (distanceToGoal < goalPrecisionSqr) {
				goalReached = true
				return@any true
			}

			if (!GeoHelper.contains(missileLevel.area, currPosition)) {
				// border hit
				hasCrashed = true
				return@any true
			}
			val obstacleHit = missileLevel.obstacles.any { GeoHelper.intersect(prevPosition, currPosition, it) }
			if (obstacleHit) {
				// obstacle hit
				hasCrashed = true
				return@any true
			}

			if (smallestDistance > distanceToGoal) {
				smallestDistance = distanceToGoal
			}

			false
		}

		m.positions = positions
		m.hasCrashed = hasCrashed

		val finalDistance: Double
		if (goalReached) {
			m.goalReached = true
			m.stepToGoal = positions.size
			finalDistance = 0.0
		} else {
			m.goalReached = false
			m.stepToGoal = null
			finalDistance = Math.sqrt(smallestDistance)
		}
		m.distanceToGoal = finalDistance

		val unusedAngles = m.angles.size - positions.size
		val score =
			if (goalReached) {
				1000 / (positions.size.toDouble() * (1 + unusedAngles * 0.02))
			} else {
				if (hasCrashed) {
					-finalDistance// * 10.0
				} else {
					-finalDistance
				}// * (1 + unusedAngles * 0.05)
			}

		m.fitnessScore = score
	}
}
