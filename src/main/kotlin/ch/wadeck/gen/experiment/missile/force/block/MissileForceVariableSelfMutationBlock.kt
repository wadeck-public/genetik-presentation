package ch.wadeck.gen.experiment.missile.force.block

import ch.wadeck.gen.common.generation.other.SelfMutationBlock
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.experiment.missile.force.MissileForceMutant

class MissileForceVariableSelfMutationBlock(
	val coefPerGene: Double,
	val coefAngleModification: Double,
	val coefAngleSizeModification: Double ,
	val coefForceModification: Double
) : SelfMutationBlock<MissileForceMutant>() {
	override fun selfMutation(mutant: MissileForceMutant): MissileForceMutant {
		var hasMutated = false
		val allAngles:MutableList<Double> = mutant.angles.map {
			if (generation.random.nextDouble() < coefPerGene) {
				val newAngle = it + generation.random.nextGaussian() * coefAngleModification
				hasMutated = true
				MathHelper.cyclicClamp(newAngle, 0.0, 1.0)
			} else {
				it
			}
		}.toMutableList()

		val newAngleSize = MathHelper.intRound(allAngles.size * (1 + generation.random.nextGaussian() * coefAngleSizeModification))
		if(allAngles.size > newAngleSize){
			val delta = allAngles.size - newAngleSize
			repeat(delta){
				allAngles.removeAt(allAngles.size - 1)
			}
		}else if(allAngles.size < newAngleSize){
			val delta = newAngleSize - allAngles.size
			repeat(delta){
				allAngles += generation.random.nextDouble()
			}
		}

		return if (hasMutated) {
			MissileForceMutant(allAngles, mutant.force * (1 + generation.random.nextGaussian() * coefForceModification))
		} else {
			mutant
		}
	}
}

