package ch.wadeck.gen.experiment.missile.force

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.geom.DoublePoint

class MissileForceMutant(
	// variable list of cyclic gene01
	val angles: List<Double>,
	// gene floating starting at 0.3
	val force: Double
) : Mutant<MissileForceMutant>() {
	var goalReached: Boolean? = null
	var distanceToGoal: Double? = null
	var stepToGoal: Int? = null
	var positions: List<DoublePoint> = emptyList()
	var hasCrashed: Boolean? = null

	override val familyClass: String by lazy {
		"force:${force}, anglesSize:${angles.size}"
	}

	override fun getGeneAsString(): String =
		angles.toString()

	override fun toSimplifiedString(): String =
		"force:${MathHelper.approx(force)}, angles(${angles.size}):" +
			if (angles.size > 10) {
				angles.subList(0, 10).map { MathHelper.approx(it) }.toString()
			} else {
				angles.map { MathHelper.approx(it) }.toString()
			}

	override fun toExtendedString(): String =
		angles.toString()

	override fun copy(): MissileForceMutant =
		MissileForceMutant(angles.toList(), force)
}
