package ch.wadeck.gen.experiment.missile.force.block

import ch.wadeck.gen.common.generation.MutantGeneratorBlock
import ch.wadeck.gen.experiment.missile.force.MissileForceMutant

class MissileForceMutantGeneratorBlock(
	val numOfAngle: Int,
	val force: Double
) : MutantGeneratorBlock<MissileForceMutant>() {
	override fun generateNew(): MissileForceMutant {
		val angles = (1..numOfAngle).map { generation.random.nextDouble() }
		return MissileForceMutant(angles, force)
	}
}
