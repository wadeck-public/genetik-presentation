package ch.wadeck.gen.experiment.missile.common

import ch.wadeck.gen.common.util.AssertHelper
import ch.wadeck.gen.common.util.GeoHelper
import ch.wadeck.gen.common.util.geom.DoublePoint
import ch.wadeck.gen.common.util.geom.DoubleRect

class MissileLevel(val start: DoublePoint, val goal: DoublePoint,
                   val area: DoubleRect,
                   val obstacles: List<DoubleRect> = emptyList()) {
	init {
		AssertHelper.isTrue(GeoHelper.contains(area, start))
		AssertHelper.isTrue(GeoHelper.contains(area, goal))
		AssertHelper.isFalse(obstacles.any {
			// no obstacle contain the start or the goal
			GeoHelper.contains(it, start) ||
				GeoHelper.contains(it, goal)
		})
	}
}
