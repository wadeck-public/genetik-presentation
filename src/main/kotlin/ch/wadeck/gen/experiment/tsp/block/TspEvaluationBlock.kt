package ch.wadeck.gen.experiment.tsp.block

import ch.wadeck.gen.common.generation.EvaluationBlock
import ch.wadeck.gen.experiment.tsp.CityMap
import ch.wadeck.gen.experiment.tsp.TspMutant

class TspEvaluationBlock(val cityMap: CityMap) : EvaluationBlock<TspMutant>() {
	override fun computeFitness(mutants: List<TspMutant>) {
		mutants.forEach { m ->
			m.distance = cityMap.distanceThroughAllIndices(m.cityIndices)
			m.fitnessScore = -m.distance!!
		}
	}
}
