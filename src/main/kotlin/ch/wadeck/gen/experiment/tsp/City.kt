package ch.wadeck.gen.experiment.tsp

data class City(val name: String, val x: Int, val y: Int) {
	fun distanceTo(that: City) = Math.sqrt(
		((this.x - that.x) * (this.x - that.x) + (this.y - that.y) * (this.y - that.y)).toDouble()
	)
}