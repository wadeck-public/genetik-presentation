package ch.wadeck.gen.experiment.tsp

import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.evolution.ReproductionBetweenDifferentEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.AddRandomUsingMomentumEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.KeepTheBestUsingMomentumEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.MutateSomeGenesEvolutionBlock
import ch.wadeck.gen.common.generation.goal.NeverOkBlock
import ch.wadeck.gen.common.generation.other.selection.WeightedSelectionBlock
import ch.wadeck.gen.common.gui.ChartFrame
import ch.wadeck.gen.common.gui.chart.LineChart
import ch.wadeck.gen.common.gui.panel.ChartPanel
import ch.wadeck.gen.common.gui.panel.CityMapPanel
import ch.wadeck.gen.common.gui.panel.QualityChartPanel
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.RandomHelper
import ch.wadeck.gen.common.util.geom.DoublePoint
import ch.wadeck.gen.common.util.geom.IntRect
import ch.wadeck.gen.experiment.tsp.block.TspEvaluationBlock
import ch.wadeck.gen.experiment.tsp.block.TspMutantGeneratorBlock
import ch.wadeck.gen.experiment.tsp.block.TspSelfMutationBlock
import ch.wadeck.gen.experiment.tsp.block.crossover.TspCrossoverBlock
import java.awt.GridLayout
import java.util.*
import javax.swing.JPanel

class TSP_02_Previous {
	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			TSP_02_Previous()
				.start()
		}
	}

	//	lateinit var frame: ChartFrame
	val cityMap: CityMap

	val MUTANT_PER_GENERATION = 100
	//		val NUM_CITIES = 15
//	val NUM_CITIES = 10
	val NUM_CITIES = 25
	val random: Random

	val generation: MutantGeneration<TspMutant>

	val frame: ChartFrame
	val NEW_RANDOM_RATIO_PER_GENERATION = 0.02

	val updateDrawing: () -> Unit

	init /* prepare data */ {
		val mainSeed = 100000L
		val seedRandom = Random(mainSeed)

		random = Random(seedRandom.nextLong())
//		val testDataRandom = new Random(seedRandom.nextLong())
		/**
		 * 400, c=15, gen=100
		 * best: 189.5 ? (G, C, N, I, L, H, A, F, B, D, E, J, O, K, M)
		 */
		val testDataRandom = Random(400)

		val WORLD_BOUNDARIES = IntRect(0, 0, 100, 100)
//		val WORLD_BOUNDARIES = IntRect(0, 0, 100, 20)
//		val WORLD_BOUNDARIES = IntRect(0, 0, 100, 20)
		val MIN_DISTANCE = 10

		val cities = mutableListOf<City>()
		var cityName = 'A'
		repeat(NUM_CITIES) {
			var good = false
			var newCity: City? = null
			while (!good) {
				val location = RandomHelper.randomPointInRect(WORLD_BOUNDARIES, testDataRandom)
				newCity = City(cityName.toString(), location.x, location.y)
				good = cities.all { city -> city.distanceTo(newCity) > MIN_DISTANCE }
			}

			cityName++
			cities += newCity!!
		}

		cityMap = CityMap(cities, WORLD_BOUNDARIES)
		printCity(WORLD_BOUNDARIES.maxX, WORLD_BOUNDARIES.maxY, cityMap)
	}

	init /* prepare generation */ {
		val mutantGenerator = TspMutantGeneratorBlock(cityMap)

		val evaluationBlock = TspEvaluationBlock(cityMap)

		val goalBlock = NeverOkBlock<TspMutant>()

		val evolutionBlock =
			KeepTheBestUsingMomentumEvolutionBlock(
				0.0, 0.5,
				0.4,
				TspSelfMutationBlock(0.06),
				AddRandomUsingMomentumEvolutionBlock(
					0.1, 0.4,
					MutateSomeGenesEvolutionBlock(
						TspSelfMutationBlock(0.01),
						ReproductionBetweenDifferentEvolutionBlock(
							WeightedSelectionBlock(),
							TspCrossoverBlock()
						)
					)
				)
			)

		generation = MutantGeneration(
			MUTANT_PER_GENERATION,
			random,
			mutantGenerator,
			evaluationBlock,
			goalBlock,
			evolutionBlock
		)
	}

	init /* prepare view */ {
		val cityMapPanel = CityMapPanel(cityMap)
		val qualityPanel = QualityChartPanel()

		updateDrawing = {
			qualityPanel.updateAndRepaint(generation)
			updateVisualizationChart(cityMapPanel)
			cityMapPanel.repaint()
		}

		frame = ChartFrame("TSP 02 - Previous optimizations")

		val panel = JPanel(GridLayout())
		panel.add(qualityPanel)
		panel.add(cityMapPanel)
		frame.setMainComponent(panel)

		frame.addButton("Next") {
			generation.nextGeneration()
			generation.computeFitness()
			updateDrawing()
			printStep()
		}
		frame.addButton("Next 10") {
			repeat(10) {
				generation.nextGeneration()
				generation.computeFitness()
				updateDrawing()
				printStep()
			}
		}
		frame.addButton("Direct 10") {
			repeat(10) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			updateDrawing()
			printStep()
		}
		frame.addButton("Next 50") {
			repeat(50) {
				generation.nextGeneration()
				generation.computeFitness()
				updateDrawing()
				printStep()
			}
		}
		frame.addButton("Direct 50") {
			repeat(50) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			updateDrawing()
			printStep()
		}
		frame.addButton("Direct 500") {
			val start = System.currentTimeMillis()
			repeat(500) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			val end = System.currentTimeMillis()
			updateDrawing()
			printStep()
			println("${end - start}ms")
		}
		frame.addButton("Direct 10000") {
			val start = System.currentTimeMillis()
			repeat(10000) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			val end = System.currentTimeMillis()
			updateDrawing()
			printStep()
			println("${end - start}ms")
		}
	}

//	// normalized
//	private fun updateQualityChart(chart: ChartPanel, best: LineChart, average: LineChart, worst: LineChart) {
//		best.points += DoublePoint(generation.generationNo.toDouble(), generation.mutants.first().normalizedScore)
//
//		// rounded to the "worse" value
//		val randomAmount = MathHelper.intCeil(generation.mutantPerGeneration * NEW_RANDOM_RATIO_PER_GENERATION)
//		val nonRandomMutants = generation.mutantPerGeneration - randomAmount
//		average.points += DoublePoint(
//			generation.generationNo.toDouble(),
//			(generation.mutants.subList(0, nonRandomMutants).sumByDouble { it.normalizedScore } / nonRandomMutants)
//		)
//
//		val worstMutantBeforeRandom = generation.mutants[nonRandomMutants]
//		worst.points += DoublePoint(generation.generationNo.toDouble(), worstMutantBeforeRandom.normalizedScore)
//
//		chart.expandIfRequired(worst.points.last())
//	}

	private fun updateQualityChart(chart: ChartPanel, best: LineChart, average: LineChart, worst: LineChart) {
		best.points += DoublePoint(generation.generationNo.toDouble(), generation.mutants.first().distance!!)

		// rounded to the "worse" value
		val randomAmount = MathHelper.intCeil(generation.mutantPerGeneration * NEW_RANDOM_RATIO_PER_GENERATION)
		val nonRandomMutants = generation.mutantPerGeneration - randomAmount
		average.points += DoublePoint(
			generation.generationNo.toDouble(),
			(generation.mutants.subList(0, nonRandomMutants).sumByDouble { it.distance!! } / nonRandomMutants)
		)

		val worstMutantBeforeRandom = generation.mutants[nonRandomMutants]
		worst.points += DoublePoint(generation.generationNo.toDouble(), worstMutantBeforeRandom.distance!!)

		chart.expandIfRequired(worst.points.last())
	}

	private fun updateVisualizationChart(cityMapPanel: CityMapPanel) {
		cityMapPanel.result = generation.mutants[0].cityIndices
	}

	private fun printStep() {
		println("\t${generation.generationNo}, best: dist=${generation.mutants[0].distance}, cities=${indicesToCities(generation.mutants[0].cityIndices)}")
	}

	private fun indicesToCities(indices: IntArray) = indices.map { index -> cityMap.cities[index].name }

	private fun printCity(width: Int, height: Int, cityMap: CityMap) {
		val cells = mutableListOf<Char>()
		repeat(width * height) { cells += '.' }

		cityMap.cities.forEach { city ->
			cells[city.x + city.y * width] = city.name[0]
		}

		repeat(height) { h ->
			System.out.println(cells.subList(h * width, ((h + 1) * width)).joinToString(""))
		}
	}

	fun start() {
		generation.computeFitness()
		printStep()
		updateDrawing()
		frame.show()
	}
}

