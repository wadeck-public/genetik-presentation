package ch.wadeck.gen.experiment.tsp

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.util.MathHelper

class TspMutant(
	// gene representing the combination of a list of indices
	vararg val cityIndices: Int
) : Mutant<TspMutant>() {
	constructor(cityIndicesList: List<Int>) : this(*cityIndicesList.toIntArray())

	var distance: Double? = null

	override fun copy(): TspMutant {
		return TspMutant(*cityIndices)
	}

	override val familyClass: String =
		"#cities=${cityIndices.size}"

	override fun getGeneAsString(): String =
		cityIndices.toString()

	override fun toSimplifiedString(): String =
		"dist=${distance?.let { MathHelper.approx(it) } ?: "not computed yet"}, cityIndices=${cityIndices}"

	override fun toExtendedString(): String =
		toSimplifiedString()
}
