package ch.wadeck.gen.experiment.tsp.block

import ch.wadeck.gen.common.generation.MutantGeneratorBlock
import ch.wadeck.gen.experiment.tsp.CityMap
import ch.wadeck.gen.experiment.tsp.TspMutant

class TspMutantGeneratorBlock(val cityMap: CityMap) : MutantGeneratorBlock<TspMutant>() {
	override fun generateNew(): TspMutant {
		val indices = List(cityMap.total, { i -> i })
		return TspMutant(indices.shuffled(generation.random))
	}
}
