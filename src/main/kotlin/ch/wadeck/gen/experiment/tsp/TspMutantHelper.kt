package ch.wadeck.gen.experiment.tsp

object TspMutantHelper {
	fun swap(indices:MutableList<Int>, indexA:Int, indexB:Int) {
		indices[indexA] = indices[indexB].also { indices[indexB] = indices[indexA] }
	}
}