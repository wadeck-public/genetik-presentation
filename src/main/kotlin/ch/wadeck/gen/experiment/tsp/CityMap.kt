package ch.wadeck.gen.experiment.tsp

import ch.wadeck.gen.common.util.AssertHelper
import ch.wadeck.gen.common.util.geom.IntRect

data class CityMap(val cities: List<City>, val boundary:IntRect) {
	val total = cities.size
	private val pathToDistanceCache = mutableMapOf<String, Double>()

	fun distanceThroughAllIndices(indices: IntArray): Double =
		distanceThroughAllIndices(indices.asList())

	fun distanceThroughAllIndices(indices: List<Int>): Double {
		var score = 0.0
		repeat(total - 1) { i ->
			score += distanceBetweenIndices(indices[i], indices[i + 1])
		}
		return score
	}

	fun distanceBetweenIndices(a: Int, b: Int): Double {
		AssertHelper.inRange(a, 0, total)
		AssertHelper.inRange(b, 0, total)

		return getDistanceBetweenIndicesUsingCache(a, b)
	}

	private fun getDistanceBetweenIndicesUsingCache(a: Int, b: Int): Double {
		val path = getPath(a, b)

		return pathToDistanceCache.getOrPut(path) {
			cities[a].distanceTo(cities[b])
		}
	}

	private fun getPath(a: Int, b: Int): String {
		if (a < b) {
			return "${a}_${b}"
		} else {
			return "${b}_${a}"
		}
	}
}
