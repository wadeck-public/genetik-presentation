package ch.wadeck.gen.experiment.tsp.block.crossover

import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.experiment.tsp.TspMutant

class TspCrossoverBlock : CrossoverBlock<TspMutant>() {
	override fun crossover(parentA: TspMutant, parentB: TspMutant): TspMutant {
		val thisIndices = parentA.cityIndices
		val thatIndices = parentB.cityIndices

		val total = thisIndices.size

		val thresholdIndex = generation.random.nextInt(thisIndices.size)
		// at minimum we take one from father and one from mother
		val cutSize = 1 + generation.random.nextInt(thisIndices.size - 2)
		val resultingGenes = mutableListOf<Int>()
		repeat(cutSize) { c ->
			val fromFather = (thresholdIndex + c) % total
			resultingGenes += thisIndices[fromFather]
		}
		var currentMotherIndex = 0
		repeat(total - cutSize) {
			while (resultingGenes.contains(thatIndices[currentMotherIndex])) {
				currentMotherIndex++
			}
			resultingGenes += thatIndices[currentMotherIndex]
			currentMotherIndex++
		}

		return TspMutant(resultingGenes)
	}
}
