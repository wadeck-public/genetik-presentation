package ch.wadeck.gen.experiment.tsp.block

import ch.wadeck.gen.common.generation.other.SelfMutationBlock
import ch.wadeck.gen.common.util.RandomHelper
import ch.wadeck.gen.experiment.tsp.TspMutant
import ch.wadeck.gen.experiment.tsp.TspMutantHelper

class TspSelfMutationBlock(val coefPerGene: Double) : SelfMutationBlock<TspMutant>() {
	override fun selfMutation(mutant: TspMutant): TspMutant {
		val newIndices: MutableList<Int> = mutant.cityIndices.toMutableList()
		val total = mutant.cityIndices.size

		var hasMutated = false
		repeat(total) { i ->
			if (generation.random.nextDouble() < coefPerGene) {
				hasMutated = true
				TspMutantHelper.swap(newIndices, i, RandomHelper.randomRange(0, total, i, generation.random))
			}
		}

		return if (hasMutated) {
			TspMutant(newIndices)
		} else {
			mutant
		}
	}
}
