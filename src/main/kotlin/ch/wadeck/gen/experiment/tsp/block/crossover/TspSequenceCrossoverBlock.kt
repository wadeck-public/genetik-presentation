package ch.wadeck.gen.experiment.tsp.block.crossover

import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.experiment.tsp.TspMutant

class TspSequenceCrossoverBlock : CrossoverBlock<TspMutant>() {
	override fun crossover(parentA: TspMutant, parentB: TspMutant): TspMutant {
		val thisIndices = parentA.cityIndices
		val thatIndices = parentB.cityIndices

		val total = thisIndices.size
		val cutSize = 1 + generation.random.nextInt(total - 2)

		// where we cut from the parent, [0, total - cutSize]
		val cutIndex = generation.random.nextInt(total - cutSize)
		// where we put it in the child, [0, total - cutSize]
		val childIndex = generation.random.nextInt(total - cutSize)

		val cutGenes = mutableListOf<Int>()
		repeat(cutIndex) { i ->
			cutGenes += thisIndices[cutIndex + i]
		}

		val remainingPart = mutableListOf<Int>()

		var currentMotherIndex = 0
		repeat(total - cutSize) {
			while (cutGenes.contains(thatIndices[currentMotherIndex])) {
				currentMotherIndex++
			}
			remainingPart += thatIndices[currentMotherIndex]
			currentMotherIndex++
		}

		val resultingGenes = remainingPart.subList(0, childIndex) + cutGenes + remainingPart.subList(childIndex, remainingPart.size)
		return TspMutant(resultingGenes)
	}
}
