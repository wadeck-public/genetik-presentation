package ch.wadeck.gen.experiment.citation

import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.evolution.PureRandomEvolutionBlock
import ch.wadeck.gen.common.generation.goal.NeverOkBlock
import ch.wadeck.gen.common.gui.ChartFrame
import ch.wadeck.gen.common.gui.panel.QualityChartPanel
import ch.wadeck.gen.experiment.citation.block.CitationMutantGeneratorBlock
import ch.wadeck.gen.experiment.citation.block.CitationSumOfDifferenceEvaluationBlock
import java.awt.GridLayout
import java.util.*
import javax.swing.JPanel

class CitationVisual_01_RandomWalk {
	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			CitationVisual_01_RandomWalk()
				.start()
		}
	}

	val targetCitation: CitationLevel

	val MUTANT_PER_GENERATION = 100
	//	val MUTANT_PER_GENERATION = 20
	val random: Random

	val generation: MutantGeneration<CitationMutant>

	val frame: ChartFrame
	val NEW_RANDOM_RATIO_PER_GENERATION = 0.02

	val updateDrawing: () -> Unit

	init /* prepare data */ {
		val mainSeed = 100000L
		val seedRandom = Random(mainSeed)

		random = Random(seedRandom.nextLong())

//		targetCitation = CitationLevel(
//			"La mutation, c'est la clé de notre évolution.",
//			'a'.until('z') + 'A'.until('Z') + listOf(' ', '.', ',', '\'') + listOf('à', 'à', 'â', 'ä', 'é', 'è', 'ê', 'ë', 'ï', 'î', 'ô', 'û', 'ü')
//		)
		targetCitation = CitationLevel(
			"la mutation, c'est la clef de notre evolution.",
			'a'.until('z') + listOf(' ', '.', ',', '\'')
		)

//		targetCitation = CitationLevel(
//			"Les espèces qui survivent ne sont pas les espèces les plus fortes, ni les plus intelligentes, mais celles qui s'adaptent le mieux aux changements.",
//			'a'.until('z') + 'A'.until('Z') + listOf(' ', '.', ',', '\'') + listOf('à', 'à', 'â', 'ä', 'é', 'è', 'ê', 'ë', 'ï', 'î', 'ô', 'û', 'ü')
//		)
//		targetCitation = CitationLevel(
//			"les especes qui survivent ne sont pas les especes les plus fortes, ni les plus intelligentes, mais celles qui s'adaptent le mieux aux changements.",
//			'a'.until('z') + listOf(' ', '.', ',', '\'')
//		)
	}

	init /* prepare generation */ {
		val mutantGenerator = CitationMutantGeneratorBlock(targetCitation)

		val evaluationBlock = CitationSumOfDifferenceEvaluationBlock(targetCitation)

		val goalBlock = NeverOkBlock<CitationMutant>()

		val evolutionBlock =
			PureRandomEvolutionBlock<CitationMutant>()

		generation = MutantGeneration(
			MUTANT_PER_GENERATION,
			random,
			mutantGenerator,
			evaluationBlock,
			goalBlock,
			evolutionBlock
		)
	}

	init /* prepare view */ {
		val qualityPanel = QualityChartPanel()

		val citationPanel = CitationPanel(18)
//		val citationPanel = ChartPanel()
//		val citationDrawer = CitationDrawer()

//		citationPanel.charts += citationDrawer
//		citationPanel.expandIfRequired(100, 100)

		updateDrawing = {
			qualityPanel.updateAndRepaint(generation)
//			citationDrawer.update(generation)
			citationPanel.updateAndRepaint(generation.bestMutant, generation.mutants.subList(1, 10))
		}

		frame = ChartFrame("Citation 01 - Random Walk")

		val panel = JPanel(GridLayout())
		panel.add(qualityPanel)
		panel.add(citationPanel)
		frame.setMainComponent(panel)

		frame.addButton("Next") {
			generation.nextGeneration()
			generation.computeFitness()
			updateDrawing()
			printStep()
		}
		frame.addButton("Next 10") {
			repeat(10) {
				generation.nextGeneration()
				generation.computeFitness()
				updateDrawing()
				printStep()
			}
		}
		frame.addButton("Direct 10") {
			repeat(10) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			updateDrawing()
			printStep()
		}
		frame.addButton("Next 50") {
			repeat(50) {
				generation.nextGeneration()
				generation.computeFitness()
				updateDrawing()
				printStep()
			}
		}
		frame.addButton("Direct 50") {
			repeat(50) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			updateDrawing()
			printStep()
		}
		frame.addButton("Direct 500") {
			val start = System.currentTimeMillis()
			repeat(500) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			val end = System.currentTimeMillis()
			updateDrawing()
			printStep()
			println("${end - start}ms")
		}
		frame.addButton("Direct 10000") {
			val start = System.currentTimeMillis()
			repeat(10000) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			val end = System.currentTimeMillis()
			updateDrawing()
			printStep()
			println("${end - start}ms")
		}
	}

	private fun printStep() {
		val best = generation.mutants.first()

//		val accumulatedCorrect = generation.bestMutant.onlyCorrectCharacters().toMutableList()
//		generation.mutants.subList(1, generation.mutantPerGeneration).forEach {
//			it.onlyCorrectCharacters().forEachIndexed { i, c ->
//				if (c != '_' && accumulatedCorrect[i] == '_') {
//					accumulatedCorrect[i] = c
//				}
//			}
//		}

		println("\t${generation.generationNo}, best: score=${best.fitnessScore}, normalized=${best.toSimplifiedString()}")
//		println("\t${generation.generationNo}, best: score=${best.numCorrect}, accumulatedScore=${accumulatedCorrect.sumBy {
//			if (it == '_') {
//				0
//			} else {
//				1
//			}
//		}}, accumulatedCorrect=${accumulatedCorrect.joinToString("")}, extract=${best.toSimplifiedString()}")
	}

	fun start() {
		generation.computeFitness()
		printStep()
		updateDrawing()
		frame.show()
	}
}
