package ch.wadeck.gen.experiment.citation.block

import ch.wadeck.gen.common.generation.other.SelfMutationBlock
import ch.wadeck.gen.common.util.RandomHelper
import ch.wadeck.gen.experiment.citation.CitationLevel
import ch.wadeck.gen.experiment.citation.CitationMutant

class CitationSelfMutationBlock(
	val coefPerGene: Double,
	val citationLevel: CitationLevel
) : SelfMutationBlock<CitationMutant>() {
	override fun selfMutation(mutant: CitationMutant): CitationMutant {
		var hasMutated = false
		val newAttempt = mutant.textAttempt.toMutableList().map {
			if (generation.random.nextDouble() < coefPerGene) {
				hasMutated = true
				RandomHelper.pickSingleRandomFrom(citationLevel.possibleCharacters, generation.random)
			} else {
				it
			}
		}

		return if (hasMutated) {
			CitationMutant(newAttempt)
		} else {
			mutant
		}
	}
}

