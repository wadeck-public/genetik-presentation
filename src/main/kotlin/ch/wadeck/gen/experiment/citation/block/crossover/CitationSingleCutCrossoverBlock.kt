package ch.wadeck.gen.experiment.citation.block.crossover

import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.experiment.citation.CitationMutant

class CitationSingleCutCrossoverBlock : CrossoverBlock<CitationMutant>() {
	override fun crossover(parentA: CitationMutant, parentB: CitationMutant): CitationMutant {
		val cutStart = 1 + generation.random.nextInt(parentA.textAttempt.length - 3)
		val cutLength = 1 + generation.random.nextInt(parentA.textAttempt.length - cutStart - 1)

		val childAttempt = parentA.textAttempt.substring(0, cutStart) +
			parentB.textAttempt.substring(cutStart, cutStart + cutLength) +
			parentA.textAttempt.substring(cutStart + cutLength, parentA.textAttempt.length)

		return CitationMutant(childAttempt)
	}
}
