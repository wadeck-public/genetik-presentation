package ch.wadeck.gen.experiment.citation

import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.gui.Drawer
import ch.wadeck.gen.common.gui.Kolor
import ch.wadeck.gen.common.gui.chart.Chart

class CitationDrawer : Chart {
	var bestMutant: CitationMutant? = null
	var otherMutants: List<CitationMutant> = emptyList()

	fun update(generation:MutantGeneration<CitationMutant>){
		bestMutant = generation.bestMutant
		otherMutants = generation.mutants.subList(1, 10)
	}

	private var padding = 0.0
	private var margin = 0.0
	private var length = 0
	private var usableLength = 0.0
	private var widthPerLetter = 0.0

	override fun draw(d: Drawer) {
		if(bestMutant == null){
			return
		}

		// assume 100x100
		padding = 0.2
		margin = 2.0
		length = bestMutant!!.textAttempt.length
		usableLength = (100 - margin * 2) - (length - 1) * padding
		widthPerLetter = usableLength / length.toDouble()

		d.stroke(2f)
		displayCitationAt(d, 93.0, bestMutant!!)

		d.stroke(1f)
		otherMutants.forEachIndexed { index, m ->
			val ratio = 1 - index / otherMutants.size.toDouble()
			displayCitationAt(d, 5 + 75 * ratio, m)
		}
	}

	private fun displayCitationAt(d:Drawer, y:Double, mutant:CitationMutant){
		mutant.textAttempt.forEachIndexed { index, c ->
			val x = margin + index * (widthPerLetter + padding) - padding + widthPerLetter * 0.5
			if(mutant.correctChars!![index]){
				d.color(Kolor.DARK_GREEN)
			}else{
				d.color(Kolor.LIGHT_RED)
			}
			d.drawTextCentered(x, y, c.toString())
		}
	}
}
