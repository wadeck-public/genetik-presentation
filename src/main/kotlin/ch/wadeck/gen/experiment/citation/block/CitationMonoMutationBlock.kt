package ch.wadeck.gen.experiment.citation.block

import ch.wadeck.gen.common.generation.other.SelfMutationBlock
import ch.wadeck.gen.common.util.RandomHelper
import ch.wadeck.gen.experiment.citation.CitationLevel
import ch.wadeck.gen.experiment.citation.CitationMutant

// Pure exploitation block
class CitationMonoMutationBlock(
	val citationLevel: CitationLevel
) : SelfMutationBlock<CitationMutant>() {
	override fun selfMutation(mutant: CitationMutant): CitationMutant {
		val indexOfMutation = generation.random.nextInt(mutant.textAttempt.length)
		val newAttempt = mutant.textAttempt.substring(0, indexOfMutation) +
			RandomHelper.pickSingleRandomFrom(citationLevel.possibleCharacters, generation.random) +
			mutant.textAttempt.substring(indexOfMutation + 1, mutant.textAttempt.length)

		return CitationMutant(newAttempt)
	}
}

