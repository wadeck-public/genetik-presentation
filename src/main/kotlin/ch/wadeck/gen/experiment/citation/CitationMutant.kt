package ch.wadeck.gen.experiment.citation

import ch.wadeck.gen.common.Mutant

class CitationMutant(
	val textAttempt: String
) : Mutant<CitationMutant>() {
	constructor(charArray: CharArray) : this(String(charArray))
	constructor(charList: List<Char>) : this(String(charList.toCharArray()))

	var numCorrect: Int? = null
	var correctChars: BooleanArray? = null

	override fun copy(): CitationMutant {
		return CitationMutant(textAttempt)
	}

	override val familyClass: String =
		"attempt=${textAttempt}"

	override fun getGeneAsString(): String =
		"attempt=${textAttempt}"

	override fun toSimplifiedString(): String =
		"numCorrect=${numCorrect}, attempt=${textAttempt}"

	override fun toExtendedString(): String =
		"numCorrect=${numCorrect}, attempt=${onlyCorrectCharacters()}"

	fun onlyCorrectCharacters() =
		textAttempt.mapIndexed { index, c ->
			if (correctChars!![index]) {
				c
			} else {
				'_'
			}
		}
			.joinToString("")
}
