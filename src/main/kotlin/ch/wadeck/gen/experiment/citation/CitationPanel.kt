package ch.wadeck.gen.experiment.citation

import ch.wadeck.gen.common.gui.Kolor
import java.awt.Dimension
import java.awt.Font
import java.awt.Insets
import java.awt.Point
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import javax.swing.*

class CitationPanel(var fontSize: Int) : JPanel() {
	val MAX_FONT_SIZE = 60
	val MIN_FONT_SIZE = 5

	var defaultFont = Font("Courier New", Font.PLAIN, fontSize)
	val bestTextLabel: JLabel = JLabel("-- placeholder for best --", SwingConstants.CENTER).also { it.font = defaultFont }
	val separationLabel: JLabel = JLabel("<html><span>------------</span></html>", SwingConstants.CENTER).also { it.font = defaultFont }
	val otherTextLabelList: MutableList<JLabel> = mutableListOf()

	var lineBreak: Int = 400 / fontSize

	private var bestMutant: CitationMutant? = null
	private var otherMutants: List<CitationMutant>? = null

	val mainPanel = JPanel()

	val buttonFontPlus = JButton("Font++")
	val buttonFontMinus = JButton("Font--")

	init {
		layout = null
		this.add(mainPanel)
		mainPanel.background = Kolor.WHITE
		mainPanel.layout = BoxLayout(mainPanel, BoxLayout.Y_AXIS)

		mainPanel.add(bestTextLabel)
		mainPanel.add(Box.createRigidArea(Dimension(0, 10)))
		mainPanel.add(separationLabel)
		mainPanel.add(Box.createRigidArea(Dimension(0, 10)))

		this.addComponentListener(object : ComponentAdapter() {
			override fun componentResized(e: ComponentEvent?) {
				super.componentResized(e)
				onResize()
			}

			override fun componentShown(e: ComponentEvent?) {
				super.componentShown(e)
				onResize()
			}
		})

		buttonFontPlus.addActionListener{
			fontSize = Math.min(fontSize + 1, MAX_FONT_SIZE)
			defaultFont = Font("Courier New", Font.PLAIN, fontSize)
			bestTextLabel.font = defaultFont
			separationLabel.font = defaultFont
			otherTextLabelList.forEach { it.font = defaultFont }
			onResize()
			updateLabels()
		}

		buttonFontMinus.addActionListener{
			fontSize = Math.max(fontSize - 1, MIN_FONT_SIZE)
			defaultFont = Font("Courier New", Font.PLAIN, fontSize)
			bestTextLabel.font = defaultFont
			separationLabel.font = defaultFont
			otherTextLabelList.forEach { it.font = defaultFont }
			onResize()
			updateLabels()
		}

		this.add(buttonFontPlus)
		buttonFontPlus.size = Dimension(70, 20)
		buttonFontPlus.margin = Insets(0,0,0,0)
		setComponentZOrder(buttonFontPlus, 0)

		this.add(buttonFontMinus)
		buttonFontMinus.size = Dimension(70, 20)
		buttonFontPlus.margin = Insets(0,0,0,0)
		setComponentZOrder(buttonFontMinus, 0)
	}

	private fun onResize() {
		val t = this.width / defaultFont.size.toDouble() * 1.5
		lineBreak = t.toInt()

		mainPanel.setBounds(0, 0, width, height)

		buttonFontPlus.location = Point(width - 150, height - 25)
		buttonFontMinus.location = Point(width - 75, height - 25)

		updateLabels()
	}

	fun updateLabels() {
		// local variable to ensure non-nullability
		val bestMutant = this.bestMutant
		val otherMutants = otherMutants
		if (bestMutant == null || otherMutants == null) {
			return
		}
		bestTextLabel.text = getGreenRedText(bestMutant)

		val delta = otherTextLabelList.size - otherMutants.size
		if (delta > 0) {
			// need to remove existing labels
			repeat(delta) {
				val removedLabel = otherTextLabelList.removeAt(otherTextLabelList.size - 1)
				mainPanel.remove(removedLabel)
			}
		} else if (delta < 0) {
			// need to add new labels
			repeat(-delta) {
				val newLabel = JLabel("", SwingConstants.CENTER).also { it.font = defaultFont }
				otherTextLabelList.add(newLabel)
				mainPanel.add(newLabel)
			}
		}
		otherMutants.forEachIndexed { index, m ->
			otherTextLabelList[index].text = getGreenRedText(m)
		}

		this.repaint()
	}

	fun updateAndRepaint(bestMutant: CitationMutant, otherMutants: List<CitationMutant>) {
		this.bestMutant = bestMutant
		this.otherMutants = otherMutants
		updateLabels()
	}

	private fun getGreenRedText(mutant: CitationMutant) =
		mutant.textAttempt.mapIndexed { index, c ->
			var r = if (mutant.correctChars!![index]) {
				"""<span style="color:green">$c</span>"""
			} else {
				"""<span style="color:red">$c</span>"""
			}
			if (index != 0 && index % lineBreak == 0) {
				r += "<br/>"
			}
			r
		}.joinToString("", "<html><pre>", "</pre></html>")
}
