package ch.wadeck.gen.experiment.citation.block

import ch.wadeck.gen.common.generation.MutantGeneratorBlock
import ch.wadeck.gen.common.util.RandomHelper
import ch.wadeck.gen.experiment.citation.CitationLevel
import ch.wadeck.gen.experiment.citation.CitationMutant

class CitationMutantGeneratorBlock(
	val citationLevel: CitationLevel
) : MutantGeneratorBlock<CitationMutant>() {
	override fun generateNew(): CitationMutant {
		val attempt = List(citationLevel.citation.length){
			RandomHelper.pickSingleRandomFrom(citationLevel.possibleCharacters, generation.random)
		}
		return CitationMutant(String(attempt.toCharArray()))
	}
}
