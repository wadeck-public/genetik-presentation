package ch.wadeck.gen.experiment.citation

data class CitationLevel(
	val citation: String,
	val possibleCharacters: List<Char>
)
