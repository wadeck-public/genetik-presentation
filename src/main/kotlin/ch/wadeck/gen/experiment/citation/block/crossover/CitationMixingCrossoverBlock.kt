package ch.wadeck.gen.experiment.citation.block.crossover

import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.experiment.citation.CitationMutant

class CitationMixingCrossoverBlock : CrossoverBlock<CitationMutant>() {
	override fun crossover(parentA: CitationMutant, parentB: CitationMutant): CitationMutant {
		val childAttempt = List(parentA.textAttempt.length) { i ->
			if(generation.random.nextBoolean()){
				parentA.textAttempt[i]
			}else{
				parentB.textAttempt[i]
			}
		}

		return CitationMutant(childAttempt)
	}
}
