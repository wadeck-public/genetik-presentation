package ch.wadeck.gen.experiment.citation.block

import ch.wadeck.gen.common.generation.EvaluationBlock
import ch.wadeck.gen.experiment.citation.CitationLevel
import ch.wadeck.gen.experiment.citation.CitationMutant

class CitationSumOfDifferenceEvaluationBlock(
	val citationLevel: CitationLevel
) : EvaluationBlock<CitationMutant>() {
	override fun computeFitness(mutants: List<CitationMutant>) {
		mutants.forEach { m ->
			var totalCorrect = 0
			val correctCharacters = mutableListOf<Boolean>()
			m.textAttempt.forEachIndexed { index, c ->
				if (citationLevel.citation[index] == c) {
					totalCorrect++
					correctCharacters.add(true)
				}else{
					correctCharacters.add(false)
				}
			}
			m.correctChars = correctCharacters.toBooleanArray()
			m.numCorrect = totalCorrect
			m.fitnessScore = totalCorrect / citationLevel.citation.length.toDouble()
		}
	}
}
