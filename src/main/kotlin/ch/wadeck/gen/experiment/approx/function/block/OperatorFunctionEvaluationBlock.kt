package ch.wadeck.gen.experiment.approx.function.block

import ch.wadeck.gen.common.generation.EvaluationBlock
import ch.wadeck.gen.experiment.approx.function.Function2DToApproximate
import ch.wadeck.gen.experiment.approx.function.OperatorMutant

class OperatorFunctionEvaluationBlock(
	val functionToApproximate: Function2DToApproximate
) : EvaluationBlock<OperatorMutant>() {
	override fun computeFitness(mutants: List<OperatorMutant>) {
		mutants.forEach { m ->
			val errors = functionToApproximate.evaluate(m.operator)
			m.allErrors = errors
			m.sumError = errors.sumByDouble { Math.min(1_000_000.0, (it * it)) }
			m.fitnessScore = - m.sumError!!
		}
	}
}
