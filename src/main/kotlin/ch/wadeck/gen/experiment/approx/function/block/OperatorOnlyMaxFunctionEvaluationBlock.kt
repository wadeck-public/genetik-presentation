package ch.wadeck.gen.experiment.approx.function.block

import ch.wadeck.gen.common.generation.EvaluationBlock
import ch.wadeck.gen.experiment.approx.function.Function2DToApproximate
import ch.wadeck.gen.experiment.approx.function.OperatorMutant

class OperatorOnlyMaxFunctionEvaluationBlock(
	val functionToApproximate: Function2DToApproximate
) : EvaluationBlock<OperatorMutant>() {
	override fun computeFitness(mutants: List<OperatorMutant>) {
		mutants.forEach { m ->
			val errors = functionToApproximate.evaluate(m.operator)
			m.allErrors = errors
			val maxError = errors.maxBy{ Math.abs(it) } ?: Double.MAX_VALUE
			m.sumError = Math.min(1_000_000.0, Math.abs(maxError))
			m.fitnessScore = - m.sumError!! * (1.0 + 0.1 * m.statDepth + 0.1 * m.statNumNode)
		}
	}
}
