package ch.wadeck.gen.experiment.approx.function.block.crossover

import ch.wadeck.gen.common.generation.other.CrossoverBlock
import ch.wadeck.gen.common.operator.DualOperator
import ch.wadeck.gen.common.operator.Operator
import ch.wadeck.gen.experiment.approx.function.MutationConfig
import ch.wadeck.gen.experiment.approx.function.OperatorMutant

class OperatorMutantCrossoverBlock(
	val mutationConfig: MutationConfig
) : CrossoverBlock<OperatorMutant>() {
	override fun crossover(parentA: OperatorMutant, parentB: OperatorMutant): OperatorMutant =
		OperatorMutant(mutateByTreeMerge(parentA.operator, parentB.operator, true))

	private fun mutateByTreeMerge(parentA: Operator, parentB: Operator, rootNode: Boolean = false): Operator =
		if (parentA is DualOperator && parentB is DualOperator) {
			// both are branches
			// we only consider taking fully a branch if we are not in root
			if (!rootNode && generation.random.nextDouble() < mutationConfig.chanceTreeMergeKeepFullBranch) {
				// we keep the whole sub-branch from b
				parentB.copy()
			} else {
				// we go deeper by selecting left or right sub-branch
				if (generation.random.nextBoolean()) {
					parentA.copyUsing(mutateByTreeMerge(parentA.op1, parentB.op1), parentB.op2)
				} else {
					parentA.copyUsing(parentA.op1, mutateByTreeMerge(parentA.op2, parentB.op2))
				}
			}
		} else {
			// at least one is not a branch but a leaf
			if (rootNode) {
				parentA.copy()
			} else {
				parentB.copy()
			}
		}
}
