package ch.wadeck.gen.experiment.approx.function

import ch.wadeck.gen.common.generation.MutantGeneration
import ch.wadeck.gen.common.generation.evolution.ReproductionBetweenDifferentEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.AddRandomUsingMomentumEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.KeepTheBestUsingMomentumEvolutionBlock
import ch.wadeck.gen.common.generation.evolution.meta.MutateSomeGenesEvolutionBlock
import ch.wadeck.gen.common.generation.goal.NeverOkBlock
import ch.wadeck.gen.common.generation.other.selection.RankingSelectionBlock
import ch.wadeck.gen.common.gui.ChartFrame
import ch.wadeck.gen.common.gui.chart.LineChart
import ch.wadeck.gen.common.gui.panel.ChartPanel
import ch.wadeck.gen.common.gui.panel.QualityChartPanel
import ch.wadeck.gen.common.operator.VariableNames
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.geom.DoublePoint
import ch.wadeck.gen.experiment.approx.function.block.OperatorOnlyMaxFunctionEvaluationBlock
import ch.wadeck.gen.experiment.approx.function.block.OperatorWithPrecisionMutantGeneratorBlock
import ch.wadeck.gen.experiment.approx.function.block.OperatorWithPrecisionSelfMutationBlock
import ch.wadeck.gen.experiment.approx.function.block.crossover.OperatorMutantCrossoverBlock
import java.awt.Color
import java.awt.GridLayout
import java.util.*
import javax.swing.JPanel

class FunctionApproximationAdaptativePrecisionVisual {
	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			FunctionApproximationAdaptativePrecisionVisual()
				.start()
		}
	}

	val function2DToApproximate: Function2DToApproximate
	val variableNames: VariableNames

	val MUTANT_PER_GENERATION = 100
	val random: Random

	val generation: MutantGeneration<OperatorMutant>

	val frame: ChartFrame
	val NEW_RANDOM_RATIO_PER_GENERATION = 0.02

	val updateDrawing: () -> Unit

	init /* prepare data */ {
		val mainSeed = 100000L
		val seedRandom = Random(mainSeed)

		random = Random(seedRandom.nextLong())

//		function2DToApproximate = Function2DToApproximate(-100.0, 100.0, "x", 21)
//		function2DToApproximate = Function2DToApproximate(-10.0, 10.0, "x", 21)
//		{ x -> x + 5}
//		{ x -> 5 * x * x - 15 * x + 25}
		function2DToApproximate = Function2DToApproximate(-10.0, 10.0, "x", 21)
//		{ x -> x * x - x * 1.5 }
//		{ x -> x * x - x * 1.5 + 10}
//		{ x -> x * x - x * 1.5 + 10}
		{ x -> -0.5 * x * x * x + 4 * x * x - 2 * x + 5}

		variableNames = VariableNames(setOf("x"))
	}

	init /* prepare generation */ {
		val mutationConfig = MutationConfig(precision = 0.1)

		val mutantGenerator = OperatorWithPrecisionMutantGeneratorBlock(variableNames, 2, 4, mutationConfig)

//		val evaluationBlock = OperatorFunctionEvaluationBlock(function2DToApproximate)
		val evaluationBlock = OperatorOnlyMaxFunctionEvaluationBlock(function2DToApproximate)

		val goalBlock = NeverOkBlock<OperatorMutant>()

		// groovy lib to kotlin lib: 80% !!! gain, 500ms vs 2500vs for 500 gen
		// groovy view to kotlin view: less than 100ms for 500 gen, another 80%
		val evolutionBlock =
			//CheckDuplicateEvolutionBlock(
//			KeepTheBestEvolutionBlock(
			KeepTheBestUsingMomentumEvolutionBlock(
				0.0, 0.3,
				0.5,
				OperatorWithPrecisionSelfMutationBlock(0.05, variableNames, mutationConfig),
//				AddSomeRandomEvolutionBlock(NEW_RANDOM_RATIO_PER_GENERATION,
				AddRandomUsingMomentumEvolutionBlock(
					0.1, 0.5,
					MutateSomeGenesEvolutionBlock(
//						OperatorSelfMutationBlock(0.02, variableNames, mutationConfig),
						OperatorWithPrecisionSelfMutationBlock(0.02, variableNames, mutationConfig),
						ReproductionBetweenDifferentEvolutionBlock(
//						ReproductionEvolutionBlock(
							RankingSelectionBlock(),
//							WeightedSelectionBlock(),
//							ModifiedWeightedSelectionBlock({ x -> x * x * x }),
//							WeightedParetoSelectionBlock(),
//							ParetoSelectionBlock(),
//							ModifiedWeightedParetoSelectionBlock({ x -> x * x * x }),
							OperatorMutantCrossoverBlock(mutationConfig)
						)
					)
				)
			)
//		)

		generation = MutantGeneration(
			MUTANT_PER_GENERATION,
			random,
			mutantGenerator,
			evaluationBlock,
			goalBlock,
			evolutionBlock
//			normalizationBlock = ParetoNormalizationBlock()
		)
	}

	init /* prepare view */ {
		val qualityPanel = QualityChartPanel(displayAverage = false, displayWorst = false)

		val approximationPanel = ChartPanel().also { it.centered = true }
		val targetChart = LineChart(Color(0.9f, 0.2f, 0.2f), 2f)
		targetChart.points += List(function2DToApproximate.numOfStep) { i ->
			DoublePoint(function2DToApproximate.xCoordinates[i], function2DToApproximate.expectedList[i])
		}

		val minX = targetChart.points.minBy { it.x }?.x ?: 0.0
		val minY = targetChart.points.minBy { it.y }?.y ?: 0.0
		approximationPanel.expandIfRequired(minX, minY)
		val maxX = targetChart.points.maxBy { it.x }?.x ?: 0.0
		val maxY = targetChart.points.maxBy { it.y }?.y ?: 0.0
		approximationPanel.expandIfRequired(maxX, maxY)

		val bestApproxChart = LineChart(Color(0.2f, 1.0f, 0.2f), 1.5f)
		val lastN = 90
		val nextScoreChartList = List(lastN, { i ->
			val greyLevel = 0.4f + (i / lastN.toFloat()) * 0.55f
			LineChart(Color(greyLevel, greyLevel, greyLevel))
		})

		approximationPanel.charts += nextScoreChartList.reversed()
		approximationPanel.charts += targetChart
		approximationPanel.charts += bestApproxChart

		updateDrawing = {
			qualityPanel.updateAndRepaint(generation)
			updateVisualizationChart(targetChart, bestApproxChart, nextScoreChartList)
			approximationPanel.repaint()
		}

		frame = ChartFrame("Approx")

		val panel = JPanel(GridLayout())
		panel.add(qualityPanel)
		panel.add(approximationPanel)
		frame.setMainComponent(panel)
//		frame.setMainComponent(approximationPanel)

		frame.addButton("Next") {
			generation.nextGeneration()
			generation.computeFitness()
			updateDrawing()
			printStep()
		}
		frame.addButton("Next 10") {
			repeat(10) {
				generation.nextGeneration()
				generation.computeFitness()
				updateDrawing()
				printStep()
			}
		}
		frame.addButton("Direct 10") {
			repeat(10) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			updateDrawing()
			printStep()
		}
		frame.addButton("Next 50") {
			repeat(50) {
				generation.nextGeneration()
				generation.computeFitness()
				updateDrawing()
				printStep()
			}
		}
		frame.addButton("Direct 50") {
			repeat(50) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			updateDrawing()
			printStep()
		}
		frame.addButton("Direct 500") {
			val start = System.currentTimeMillis()
			repeat(500) {
				generation.nextGeneration()
				generation.computeFitness()
			}
			val end = System.currentTimeMillis()
			updateDrawing()
			printStep()
			println("${end - start}ms")
		}
	}

//	// normalized
//	private fun updateQualityChart(chart: ChartPanel, best: LineChart, average: LineChart, worst: LineChart) {
//		best.points += DoublePoint(generation.generationNo.toDouble(), generation.mutants.first().normalizedScore)
//
//		// rounded to the "worse" value
//		val randomAmount = MathHelper.intCeil(generation.mutantPerGeneration * NEW_RANDOM_RATIO_PER_GENERATION)
//		val nonRandomMutants = generation.mutantPerGeneration - randomAmount
//		average.points += DoublePoint(
//			generation.generationNo.toDouble(),
//			(generation.mutants.subList(0, nonRandomMutants).sumByDouble { it.normalizedScore } / nonRandomMutants)
//		)
//
//		val worstMutantBeforeRandom = generation.mutants[nonRandomMutants]
//		worst.points += DoublePoint(generation.generationNo.toDouble(), worstMutantBeforeRandom.normalizedScore)
//
//		chart.expandIfRequired(worst.points.last())
//	}

	private fun updateQualityChart(chart: ChartPanel, best: LineChart, average: LineChart, worst: LineChart) {
		best.points += DoublePoint(generation.generationNo.toDouble(), generation.mutants.first().sumError!!)

		// rounded to the "worse" value
//		val randomAmount = MathHelper.intCeil(generation.mutantPerGeneration * NEW_RANDOM_RATIO_PER_GENERATION)
		val randomAmount = MathHelper.intCeil(generation.mutantPerGeneration * 0.2)
		val nonRandomMutants = generation.mutantPerGeneration - randomAmount

		// little trick to avoid too much noise due to very high error on first generation
		if(worst.points.size == 1) {
			average.points.clear()
			average.points += DoublePoint(
				generation.generationNo.toDouble() - 1,
				(generation.mutants.subList(0, nonRandomMutants).sumByDouble { it.sumError!! } / nonRandomMutants)
			)
			average.points += DoublePoint(
				generation.generationNo.toDouble(),
				(generation.mutants.subList(0, nonRandomMutants).sumByDouble { it.sumError!! } / nonRandomMutants)
			)
		}else{
			average.points += DoublePoint(
				generation.generationNo.toDouble(),
				(generation.mutants.subList(0, nonRandomMutants).sumByDouble { it.sumError!! } / nonRandomMutants)
			)
		}

		val worstMutantBeforeRandom = generation.mutants[nonRandomMutants]
		if(worst.points.size == 1){
			worst.points.clear()
			worst.points += DoublePoint(generation.generationNo.toDouble() - 1, worstMutantBeforeRandom.sumError!!)
			worst.points += DoublePoint(generation.generationNo.toDouble(), worstMutantBeforeRandom.sumError!!)
		}else{
			worst.points += DoublePoint(generation.generationNo.toDouble(), worstMutantBeforeRandom.sumError!!)
		}

		if(worst.points.size > 1){
			chart.expandIfRequired(worst.points.last())
		}
	}

	private fun updateVisualizationChart(target: LineChart, bestApprox: LineChart, next: List<LineChart>) {
		val bestMutant = generation.mutants.first()

		bestApprox.points.clear()
		// to avoid computing multiple time the same value
		bestApprox.points += target.points.mapIndexed { i, p -> DoublePoint(p.x, p.y + bestMutant.allErrors!![i]) }

		val nextMutantList = generation.mutants.subList(1, 1 + next.size)
		nextMutantList.forEachIndexed { index, m ->
			next[index].points.clear()
			next[index].points += target.points.mapIndexed { i, p -> DoublePoint(p.x, p.y + m.allErrors!![i]) }
		}
	}

	private fun printStep() {
		val best = generation.mutants.first()
		println("\t${generation.generationNo}, best: score=${best.fitnessScore}, normalized=${best.toSimplifiedString()}, func=${best.toExtendedString()}")
	}

	fun start() {
		generation.computeFitness()
		printStep()
		updateDrawing()
		frame.show()
	}
}

