package ch.wadeck.gen.experiment.approx.function

import ch.wadeck.gen.common.operator.Context
import ch.wadeck.gen.common.operator.Operator

class Function2DToApproximate(
	val minX: Double, val maxX: Double,
	val name: String = "x",
	val numOfStep: Int = 20,
	val func: (Double) -> Double
) {
	private val increment = (maxX - minX) / (numOfStep - 1)

	val xCoordinates = List(numOfStep) { i -> minX + increment * i }
	val expectedList = List(numOfStep) { i ->
		val x = minX + increment * i
		func(x)
	}

	/**
	 * Compute the errors on multiple point for the function to approximate
	 * Each error still has its sign
	 */
	fun evaluate(op: Operator): List<Double> {
		// if the context copy the map, we loose this optimization
		val variables = mutableMapOf(Pair(name, 0.0))
		val context = Context(variables)
		return List(numOfStep) { i ->
			val x = minX + increment * i
			variables[name] = x
			val actual = op.compute(context)
			val expected = expectedList[i]
			actual - expected
		}
	}
}
