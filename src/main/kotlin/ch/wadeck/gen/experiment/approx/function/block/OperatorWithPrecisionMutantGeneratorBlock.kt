package ch.wadeck.gen.experiment.approx.function.block

import ch.wadeck.gen.common.generation.MutantGeneratorBlock
import ch.wadeck.gen.common.operator.DualOperator
import ch.wadeck.gen.common.operator.NoneOperator
import ch.wadeck.gen.common.operator.Operator
import ch.wadeck.gen.common.operator.VariableNames
import ch.wadeck.gen.common.operator.impl.dual.Addition
import ch.wadeck.gen.common.operator.impl.dual.Multiplication
import ch.wadeck.gen.common.operator.impl.none.Constant
import ch.wadeck.gen.common.operator.impl.none.Variable
import ch.wadeck.gen.common.util.MathHelper
import ch.wadeck.gen.common.util.RandomHelper
import ch.wadeck.gen.experiment.approx.function.MutationConfig
import ch.wadeck.gen.experiment.approx.function.OperatorMutant

class OperatorWithPrecisionMutantGeneratorBlock(
	val variableNames: VariableNames,
	val minDepth: Int,
	val maxDepth: Int,
	val mutationConfig: MutationConfig
) : MutantGeneratorBlock<OperatorMutant>() {
	override fun generateNew(): OperatorMutant {
		val operator = randomOperator(minDepth, maxDepth)
		return OperatorMutant(operator)
	}

	private fun randomOperator(minDepth: Int, remainingDepth: Int): Operator =
		if (remainingDepth <= 0) {
			noneOperator()
		} else {
			if (minDepth > 0) {
				dualOperator(minDepth - 1, remainingDepth - 1)
			} else {
				if (generation.random.nextBoolean()) {
					noneOperator()
				} else {
					dualOperator(minDepth - 1, remainingDepth - 1)
				}
			}
		}

	private fun noneOperator(): NoneOperator =
		if (generation.random.nextBoolean()) {
			Variable(RandomHelper.pickSingleRandomFrom(variableNames.getAllNames(), generation.random))
		} else {
			Constant(MathHelper.reducePrecision(generation.random.nextDouble() * 2 - 1, mutationConfig.precision))
		}

	private fun dualOperator(minDepth: Int, remainingDepth: Int): DualOperator =
		if (generation.random.nextBoolean()) {
			Addition(randomOperator(minDepth, remainingDepth), randomOperator(minDepth, remainingDepth))
		} else {
			Multiplication(randomOperator(minDepth, remainingDepth), randomOperator(minDepth, remainingDepth))
		}
}
