package ch.wadeck.gen.experiment.approx.function

import ch.wadeck.gen.common.Mutant
import ch.wadeck.gen.common.operator.Operator
import ch.wadeck.gen.common.operator.Polynomial
import ch.wadeck.gen.common.operator.impl.none.Variable

// TODO perhaps rename the mutant to keep all the rest not linked with approximation
class OperatorMutant(val operator: Operator) : Mutant<OperatorMutant>() {
	/** statDepth, statNumVariable, statNumNode */
	private var familyClassCache: Triple<Int, Int, Int>? = null

	var allErrors: List<Double>? = null
	var sumError: Double? = null

	val statDepth: Int by lazy {
		familyClass
		familyClassCache!!.first
	}
	val statNumVariable: Int by lazy {
		familyClass
		familyClassCache!!.second
	}
	val statNumNode: Int by lazy {
		familyClass
		familyClassCache!!.third
	}

	override val familyClass: String by lazy {
		this.familyClassCache = computeStats()
		"${familyClassCache!!.first}-${familyClassCache!!.second}-${familyClassCache!!.third}"
	}

	override fun getGeneAsString(): String =
		this.operator.toString()

	override fun toSimplifiedString(): String {
		return Polynomial.buildFromOperator(this.operator).toString()
	}

	override fun toExtendedString(): String =
		this.operator.toString()

	override fun copy(): OperatorMutant =
		OperatorMutant(operator.copy())

	private fun computeStats(): Triple<Int, Int, Int> {
		var depth = 1
		var numVariable = 0
		var numNode = 1

		this.operator.traverse { currentOperator: Operator, currentDepth: Int ->
			depth = Math.max(depth, currentDepth)
			numNode++
			if (currentOperator is Variable) {
				numVariable++
			}
		}

		return Triple(depth, numVariable, numNode)
	}
}
