package ch.wadeck.gen.experiment.approx.function

// TODO perhaps put it directly inside the generation ?
data class MutationConfig(
	val weightConstantConvertToVariable: Double = 10.0,

	val weightConstantChangeValue: Double = 10.0,
	val constantChangeValueCoef: Double = 0.2,
	val constantChangeValueIncr: Double = 0.5,

	val weightConstantToDual:Double = 5.0,
	val weightVariableToDual:Double = 5.0,
	val noneToDualChanceForAddition: Double = 0.5,

	val weightConstantToMono:Double = 0.0,
	val weightVariableToMono:Double = 0.0,
	val noneToMonoChanceForInverse: Double = 0.1,

	val weightVariableConvertToConstant:Double = 10.0,
	val weightVariableChangeName:Double = 10.0,

	val weightInverseToOpposite:Double = 10.0,
	val weightInverseRemoved:Double = 30.0,

	val weightOppositeToInverse:Double = 10.0,
	val weightOppositeRemoved:Double = 30.0,

	val weightAdditionLeftOrRight:Double = 10.0,
	val weightAdditionToMultiplication:Double = 10.0,

	val weightMultiplicationLeftOrRight:Double = 10.0,
	val weightMultiplicationToAddition:Double = 10.0,

	val chanceTreeMergeKeepFullBranch:Double = 0.5,

	val precision:Double = 0.000001
)
