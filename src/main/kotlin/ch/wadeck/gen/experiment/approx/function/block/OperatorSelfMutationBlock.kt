package ch.wadeck.gen.experiment.approx.function.block

import ch.wadeck.gen.common.generation.other.SelfMutationBlock
import ch.wadeck.gen.common.operator.*
import ch.wadeck.gen.common.operator.impl.dual.Addition
import ch.wadeck.gen.common.operator.impl.dual.Multiplication
import ch.wadeck.gen.common.operator.impl.mono.Inverse
import ch.wadeck.gen.common.operator.impl.mono.Opposite
import ch.wadeck.gen.common.operator.impl.none.Constant
import ch.wadeck.gen.common.operator.impl.none.Variable
import ch.wadeck.gen.common.util.AssertHelper
import ch.wadeck.gen.common.util.RandomHelper
import ch.wadeck.gen.common.util.RandomHelper.randomlyApplyUntilAtLeastOneValid
import ch.wadeck.gen.experiment.approx.function.MutationConfig
import ch.wadeck.gen.experiment.approx.function.OperatorMutant

class OperatorSelfMutationBlock(
	val coefPerGene: Double,
	val variableNames: VariableNames,
	val mutationConfig: MutationConfig
) : SelfMutationBlock<OperatorMutant>() {
	override fun selfMutation(mutant: OperatorMutant): OperatorMutant {
		val existingRoot = mutant.operator

		var hasMutated = false
		val newRoot = existingRoot.traverseBackwardAndCopy { _, newCurrent, _ ->
			if (generation.random.nextDouble() < coefPerGene) {
				hasMutated = true
				mutate(newCurrent)
			} else {
				null
			}
		}

		return if (hasMutated) {
			OperatorMutant(newRoot)
		} else {
			mutant
		}
	}

	private fun mutate(op: Operator): Operator = when (op) {
		is NoneOperator ->
			selectMutationForNone(op)!!
		is MonoOperator ->
			selectMutationForMono(op)!!
		is DualOperator ->
			selectMutationForDual(op)!!
	}

	private fun selectMutationForNone(op: NoneOperator): Operator? = when (op) {
		is Constant ->
			randomlyApplyUntilAtLeastOneValid(generation.random,
				Pair(mutationConfig.weightConstantConvertToVariable) {
					Variable(RandomHelper.pickSingleRandomFrom(variableNames.getAllNames(), generation.random))
				},
				Pair(mutationConfig.weightConstantChangeValue) {
					mutateConstantChangeValue(op)
				},
				Pair(mutationConfig.weightConstantToDual) {
					mutateNoneToDual(op)
				},
				Pair(mutationConfig.weightConstantToMono) {
					mutateNoneToMono(op)
				}
			)
		is Variable ->
			randomlyApplyUntilAtLeastOneValid(generation.random,
				Pair(mutationConfig.weightVariableConvertToConstant) {
					mutateConstantChangeValue(Constant(0))
				},
				Pair(mutationConfig.weightVariableChangeName) {
					mutateVariableChangeName(op)
				},
				Pair(mutationConfig.weightVariableToDual) {
					mutateNoneToDual(op)
				},
				Pair(mutationConfig.weightVariableToMono) {
					mutateNoneToMono(op)
				}
			)
		else -> AssertHelper.fail("${op}(of type ${op.javaClass}) is not recognized")
	}

	private fun selectMutationForMono(op: MonoOperator): Operator? = when (op) {
		is Inverse ->
			randomlyApplyUntilAtLeastOneValid(generation.random,
				Pair(mutationConfig.weightInverseToOpposite) {
					Opposite(op.op.copy())
				},
				Pair(mutationConfig.weightInverseRemoved) {
					op.copy()
				}
			)
		is Opposite ->
			randomlyApplyUntilAtLeastOneValid(generation.random,
				Pair(mutationConfig.weightOppositeToInverse) {
					Inverse(op.op.copy())
				},
				Pair(mutationConfig.weightOppositeRemoved) {
					op.copy()
				}
			)
		else -> AssertHelper.fail("${op}(of type ${op.javaClass}) is not recognized")
	}

	private fun selectMutationForDual(op: DualOperator): Operator? = when (op) {
		is Addition ->
			randomlyApplyUntilAtLeastOneValid(generation.random,
				Pair(mutationConfig.weightAdditionLeftOrRight) {
					mutateAmputeOneChild(op)
				},
				Pair(mutationConfig.weightAdditionToMultiplication) {
					Multiplication(op.op1.copy(), op.op2.copy())
				}
			)
		is Multiplication ->
			randomlyApplyUntilAtLeastOneValid(generation.random,
				Pair(mutationConfig.weightMultiplicationLeftOrRight) {
					mutateAmputeOneChild(op)
				},
				Pair(mutationConfig.weightMultiplicationToAddition) {
					Addition(op.op1.copy(), op.op2.copy())
				}
			)
		else -> AssertHelper.fail("${op}(of type ${op.javaClass}) is not recognized")
	}

	private fun mutateConstantChangeValue(op: Constant) =
		Constant(
	mutationConfig.constantChangeValueIncr * generation.random.nextGaussian() +
			op.fixedValue *
				(1 + mutationConfig.constantChangeValueCoef * generation.random.nextGaussian())
		)

	private fun mutateNoneToDual(op: NoneOperator) =
		if (generation.random.nextDouble() < mutationConfig.noneToDualChanceForAddition) {
			Addition(op, mutateConstantChangeValue(Constant(0)))
		} else {
			Multiplication(op, mutateConstantChangeValue(Constant(1)))
		}

	private fun mutateVariableChangeName(op: Variable): Operator? {
		val otherAvailableNames = variableNames.getAllNames().filterNot { it == op.name }
		if (otherAvailableNames.isEmpty()) {
			// will try the next possible mutation
			return null
		} else {
			return Variable(RandomHelper.pickSingleRandomFrom(otherAvailableNames, generation.random))
		}
	}

	private fun mutateNoneToMono(op: NoneOperator) =
		if (generation.random.nextDouble() < mutationConfig.noneToMonoChanceForInverse) {
			Inverse(op)
		} else {
			Opposite(op)
		}

	private fun mutateAmputeOneChild(op: DualOperator) =
		if (generation.random.nextBoolean()) {
			op.op1.copy()
		} else {
			op.op2.copy()
		}
}

