package ch.wadeck.gen.common.util

import ch.wadeck.gen.common.util.MathHelper.assertClose
import org.junit.jupiter.api.Test

internal class MathHelperTest {

	@Test
	fun cyclicClamp() {
		assertClose(0.5, MathHelper.cyclicClamp(0.5, 0.0, 1.0))
		assertClose(0.5, MathHelper.cyclicClamp(0.5, 0.0, 1.0))
		// cyclic
		assertClose(0.3, MathHelper.cyclicClamp(1.3, 0.0, 1.0))
		// inclusive min
		assertClose(0.0, MathHelper.cyclicClamp(0.0, 0.0, 1.0))
		// exclusive max
		assertClose(0.0, MathHelper.cyclicClamp(1.0, 0.0, 1.0))

		assertClose(0.6, MathHelper.cyclicClamp(-0.4, 0.0, 1.0))
		assertClose(0.6, MathHelper.cyclicClamp(-2.4, 0.0, 1.0))
		assertClose(0.6, MathHelper.cyclicClamp(4.6, 0.0, 1.0))

		assertClose(2.3, MathHelper.cyclicClamp(2.3, 2.0, 3.0))
		assertClose(2.3, MathHelper.cyclicClamp(1.3, 2.0, 3.0))
		assertClose(2.3, MathHelper.cyclicClamp(3.3, 2.0, 3.0))

		assertClose(3.4, MathHelper.cyclicClamp(3.4, 2.0, 3.5))
		assertClose(2.0, MathHelper.cyclicClamp(3.5, 2.0, 3.5))
		assertClose(2.5, MathHelper.cyclicClamp(4.0, 2.0, 3.5))

		assertClose(0.6, MathHelper.cyclicClamp(2.6, -1.0, 1.0))
		assertClose(-0.6, MathHelper.cyclicClamp(-2.6, -1.0, 1.0))
		assertClose(0.8, MathHelper.cyclicClamp(-1.2, -1.0, 1.0))
	}


	@Test
	fun cyclicAvg() {
		assertClose(3.0, MathHelper.cyclicAvg(2.0, 4.0, 3.0))

		assertClose(1.5, MathHelper.cyclicAvg(0.0, 3.0, 1.5, 1.5, 1.5))
		assertClose(2.5, MathHelper.cyclicAvg(2.0, 3.0, 2.5, 2.5, 2.5))
		assertClose(2.333333, MathHelper.cyclicAvg(-1.0, 3.0, 1.5, 2.0, -0.5))

		assertClose(0.0, MathHelper.cyclicAvg(0.0, 1.0, 0.1, 0.9))
		assertClose(0.7, MathHelper.cyclicAvg(0.0, 1.0, 0.8, 0.6))
		assertClose(0.9, MathHelper.cyclicAvg(0.0, 1.0, 0.1, 0.7))
		assertClose(0.05, MathHelper.cyclicAvg(0.0, 1.0, 1.2, 0.9))

		assertClose(3.9, MathHelper.cyclicAvg(2.0, 4.0, 2.1, 3.7))
		assertClose(3.9, MathHelper.cyclicAvg(2.0, 4.0, 4.1, 3.7))
	}
}
