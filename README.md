Link to have access to the [List of runnable demo](https://gitlab.com/wadeck-public/genetik-presentation/wikis/List-of-runnable-demo)

Link to the presentation [Introduction to Genetic Algorithms](https://docs.google.com/presentation/d/1PRRzrYvLgDXS8y_UQBQ7dyF5Uje7AVBQxVobivzKggk/edit?usp=sharing)

## HowTo

### Build the demo jars

Only available for IntelliJ
Copy the `artifacts` folder inside `.idea` folder and then you can just `Build > Build Artifacts > All Artifacts > Build`

Works with IntelliJ Community Edition with Kotlin plugin 1.2.31.

### Exceptions

In case of : `Exception in thread "main" java.lang.VerifyError: Uninitialized object exists on backward branch 254`
=> Use a more recent version of Java (does not work for me with early version of 1.8.0_05 but works with 1.8.0_162)
